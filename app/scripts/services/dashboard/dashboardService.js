angular.module('minovateApp')

.factory('DashboardService', function($http, $cookies) {

  return {

    get_data: function(subdomain, domain, data) {

      return $http({
        method: 'Get',
        crossDomain: true,
        dataType: "JSONP",
        url: "http://" + subdomain + "." + domain + "/api/dashboards?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });
    },
  }
});
