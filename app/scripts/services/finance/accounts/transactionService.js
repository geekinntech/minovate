angular.module('minovateApp')

.factory('Transaction', function($http, $cookies) {
  return {

    index: function(subdomain, domain, params, page_no) {

      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/transactions?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=index&in=transactions_index&perform_action=transactions.index' + '&t_type=' + params + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id + '&page=' + page_no,
        //url: 'http://scitech.scitech.hr.dev:3002/api/transactions',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
    get: function(subdomain, domain, id) {
      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=transactions_show&a=' + id + '&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        //url: 'http://scitech.scitech.hr.dev:3002/api/transactions/' + id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },


    save: function(subdomain, domain, Data) {

      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/transactions?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=index&in=transactions_index&perform_action=transactions.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id+ '&page='+1,
        //url: 'http://scitech.scitech.hr.dev:3002/api/transactions',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(Data)

      });

    },
    get_trax_contact: function(subdomain, domain, Data) {

      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        //url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=get_trax_contact&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        url: 'http://' + subdomain + '.' + domain + '/api/account_reports/get_trax_contact?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=get_trax_account&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(Data)

      });

    },
    get_trax_account: function(subdomain, domain, Data) {

      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        //url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=get_trax_account&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        url: 'http://' + subdomain + '.' + domain + '/api/account_reports/get_trax_account?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=get_trax_account&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(Data)

      });

    },
    patch: function(subdomain, domain, Data, id) {

      return $http({
        method: 'PATCH',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=update&in=transactions_update&a=' + id + '&perform_action=transactions.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        //url: 'http://scitech.scitech.hr.dev:3002/api/transactions/' + id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(Data)

      });

    },


    // destroy a comment
    destroy: function(subdomain, domain, data, id) {
      return $http({
        method: 'DELETE',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=delete&in=transactions_delete&a=' + id + '&perform_action=transactions.delete' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        //url: 'http://scitech.scitech.hr.dev:3002/api/transactions/' + id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(data)

      });
    },

    fee_collection: function(subdomain, domain, student_fee) {
      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=index&in=fee_collection&perform_action=transactions.fee_collection' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(student_fee)

      });

    },

  }
});
