angular.module('minovateApp')
  .factory('COA', function($http, $cookies) {

    return {

      index: function(subdomain, domain) {

        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/chart_of_accounts?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=coa&action_name=index&in=coa_index&perform_action=coa.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },
      get: function(subdomain, domain, id) {
        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/chart_of_accounts/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=coa&action_name=show&in=coa_show&a=' + id + '&perform_action=coa.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },


      save: function(subdomain, domain, coaData) {

        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/chart_of_accounts?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=coa&action_name=create&in=coa_create&perform_action=coa.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(coaData)
        });

      },
      patch: function(subdomain, domain, coaData, id) {

        return $http({
          method: 'PATCH',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/chart_of_accounts/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=coa&action_name=update&in=coa_update&a=' + id + '&perform_action=coa.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(coaData)

        });

      },
      destroy: function(subdomain, domain, id) {
        return $http({
          method: 'DELETE',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/chart_of_accounts/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=coa&action_name=delete&in=coa_delete&a=' + id + '&perform_action=coa.delete' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });
      },

      ajax: function(subdomain, domain, query) {

        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/chart_of_accounts/ajax?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&query=' + query,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },

    }
  })
