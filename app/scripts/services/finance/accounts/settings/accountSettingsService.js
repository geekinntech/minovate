angular.module('minovateApp')
  .factory('AccountSettings', ['$q', '$http', '$cookies', function($q, $http, $cookies) {
    return {

      // get all the selections
      save: function(subdomain, domain, data) {
        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/account_settings?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=account_settings&action_name=create&in=account_settings_create&perform_action=account_settings.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(data)
        });
      },

      get: function(subdomain, domain, id) {
        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/account_settings?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=account_settings&action_name=show&in=account_settings_show&a=' + id + '&perform_action=account_settings.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      },
    }
  }]);
