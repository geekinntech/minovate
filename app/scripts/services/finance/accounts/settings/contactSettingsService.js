angular.module('minovateApp')
  .factory('ContactSettings', ['$q', '$http', '$cookies', function($q, $http, $cookies) {
    return {

      // get all the selections

      // get all the selections
      index: function(subdomain, domain) {

        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/contacts?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=contacts&action_name=index&in=contacts_index&perform_action=contacts.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },
      get: function(subdomain, domain, id) {
        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/contacts/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=contacts&action_name=show&in=contacts_show&a=' + id + '&perform_action=contacts.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          //url: 'http://scitech.scitech.hr.dev:3002/api/contacts/' + id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },


      save: function(subdomain, domain, Data) {

        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          // url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=contacts&action_name=create&in=contacts_create&perform_action=contacts.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          url: 'http://' + subdomain + '.' + domain + '/api/contacts?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=contacts&action_name=create&in=contacts_create&perform_action=contacts.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(Data)

        });

      },
      patch: function(subdomain, domain, Data, id) {

        return $http({
          method: 'PATCH',
          crossDomain: true,
          dataType: "JSONP",
          // url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=contacts&action_name=update&in=contacts_update&a=' + id + '&perform_action=contacts.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          url: 'http://' + subdomain + '.' + domain + '/api/contacts/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=contacts&action_name=update&in=contacts_update&a=' + id + '&perform_action=contacts.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(Data)

        });

      },


      // destroy a comment
      destroy: function(subdomain, domain, data, id) {
        return $http({
          method: 'DELETE',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/contacts/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=contacts&action_name=delete&in=contacts_delete&a=' + id + '&perform_action=contacts.delete' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          //url: 'http://scitech.scitech.hr.dev:3002/api/contacts/' + id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(data)

        });
      },

      ajax: function(subdomain, domain, query, type) {

        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/contacts/ajax?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&query=' + query + '&type=' + type,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      }
    }
  }]);
