angular.module('minovateApp')
  .factory('Journal', function($http, $cookies) {
    return {

      // get all the selections
      index: function(subdomain, domain) {

        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/journals?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=journals&action_name=index&in=journals_index&perform_action=journals.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          //url: 'http://scitech.scitech.hr.dev:3002/api/journals',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },
      get: function(subdomain, domain, id) {
        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/journals/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=journals&action_name=show&in=journals_show&a=' + id + '&perform_action=journals.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          //url: 'http://scitech.scitech.hr.dev:3002/api/journals/' + id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },


      save: function(subdomain, domain, Data) {

        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/journals?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=journals&action_name=create&in=journals_create&perform_action=journals.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          //url: 'http://scitech.scitech.hr.dev:3002/api/journals',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(Data)

        });

      },
      patch: function(subdomain, domain, Data, id) {

        return $http({
          method: 'PATCH',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/journals/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=journals&action_name=update&in=journals_update&a=' + id + '&perform_action=journals.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          //url: 'http://scitech.scitech.hr.dev:3002/api/journals/' + id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(Data)

        });

      },


      // destroy a comment
      destroy: function(subdomain, domain, data, id) {
        return $http({
          method: 'DELETE',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/journals/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=journals&action_name=delete&in=journals_delete&a=' + id + '&perform_action=journals.delete' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          //url: 'http://scitech.scitech.hr.dev:3002/api/journals/' + id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(data)

        });
      }
    }
  });
