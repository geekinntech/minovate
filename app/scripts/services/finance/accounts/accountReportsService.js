angular.module('minovateApp')
  .factory('AccountReports', function($http, $cookies) {
    return {
      income_statement: function(subdomain, domain, Data) {

        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/account_reports/income_statement?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=get_trax_account&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(Data)

        });

      },

      balance_sheet: function(subdomain, domain, Data) {

        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/account_reports/balance_sheet?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=get_trax_account&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(Data)

        });

      },

      trial_balance: function(subdomain, domain, Data) {

        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/account_reports/trial_balance?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=get_trax_account&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(Data)

        });

      },
      contact_ledger: function(subdomain, domain, Data) {

        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          //url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=get_trax_contact&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          url: 'http://' + subdomain + '.' + domain + '/api/account_reports/get_trax_contact?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=get_trax_account&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(Data)

        });

      },
      account_ledger: function(subdomain, domain, Data) {

        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          //url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=get_trax_account&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          url: 'http://' + subdomain + '.' + domain + '/api/account_reports/get_trax_account?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=transactions&action_name=show&in=get_trax_account&perform_action=transactions.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(Data)

        });

      },
    }
  });
