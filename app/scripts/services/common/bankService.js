angular.module('minovateApp')

.factory('Bank', function($http, $cookies) {

  return {

    index: function(subdomain, domain) {

      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/banks?user_email=' + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=get_all_roles&module_name=role&action_name=index" + ' & branch_id = ' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
    get: function(subdomain, domain, id) {
      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/banks/' + id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },


    save: function(subdomain, domain, bankData) {

      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/banks?user_email=' + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=create_role&module_name=role&action_name=create" + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(bankData)

      });

    },
    patch: function(subdomain, domain, bankData, id) {

      return $http({
        method: 'PATCH',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/banks/' + id + "?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=update_role&a=" + data.id + "&module_name=role&action_name=update" + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(bankData)

      });

    },


    // destroy a comment
    destroy: function(subdomain, domain, id) {
      return $http({
        method: 'DELETE',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/banks/' + id + "?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=delete_role&a=" + data.id + "&module_name=role&action_name=delete" + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });
    },




  }
})
