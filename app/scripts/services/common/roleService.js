angular.module('minovateApp')
  .factory('Role', ['$q', '$http', '$cookies', function($q, $http, $cookies) {

    return {

      create: function(subdomain, domain, data) {
        return $http({
          method: 'POST',
          crossDomain: true,
          url: "http://" + subdomain + "." + domain + "/api/roles?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=create_role&module_name=role&action_name=create" + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(data)
        });
      },

      index: function(subdomain, domain) {
        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/roles?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=get_all_roles&module_name=role&action_name=index" + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });
      },

      destroy: function(subdomain, domain, data) {
        return $http({
          method: 'DELETE',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/roles/" + data.id + "?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=delete_role&a=" + data.id + "&module_name=role&action_name=delete" + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      },

      update: function(subdomain, domain, data) {
        return $http({
          method: 'PATCH',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/roles/" + data.id + "?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=update_role&a=" + data.id + "&module_name=role&action_name=update" + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(data)
        });
      }
    }
  }]);
