angular.module('minovateApp').factory('City', function($http, $cookies) {

  return {
    index: function(subdomain, domain, code) {

      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/cities?country_code=' + code,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
    },
    get_all: function(subdomain, domain) {

      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=city&action_name=index&in=city_index&perform_action=city.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
    }
  }

})
