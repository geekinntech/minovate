angular.module('minovateApp')

.factory('Wing', function($http, $cookies) {

  return {
    index: function(subdomain, domain) {
      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/wings?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=wings_index&perform_action=wings.index' + '&module_name=wings&action_name=index&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
    get_shifts: function(subdomain, domain) {
      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=wings&action_name=index&in=get_shifts&perform_action=wings.get_shifts' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
    get_wings: function(subdomain, domain, shift_id) {
      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=wings&action_name=index&in=get_wings&perform_action=wings.get_wings&shift_id=' + shift_id + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
    get_branchwings: function(subdomain, domain) {
      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=wings&action_name=index&in=get_branchwings&perform_action=wings.get_branchwings&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
    get: function(subdomain, domain, id) {
      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=wings_show&a=' + id + '&perform_action=wings.show' + '&module_name=wings&action_name=show&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },


    // save a wing (pass in comment data)
    save: function(subdomain, domain, wingData) {

      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/wings?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=wings_create&perform_action=wings.create' + '&module_name=wings&action_name=create&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(wingData)

      });

    },
    add: function(subdomain, domain, wingData) {

      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=add_wing&perform_action=wings.add_wing' + '&module_name=wings&action_name=index&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(wingData)

      });

    },
    patch: function(subdomain, domain, wingData, id) {

      return $http({
        method: 'PATCH',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/wings/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=wings_update&a=' + id + '&perform_action=wings.update' + '&module_name=wings&action_name=update&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(wingData)

      });

    },


    // destroy a comment
    destroy: function(subdomain, domain, id) {
      return $http({
        method: 'DELETE',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=wings_delete&a=' + id + '&perform_action=wings.delete' + '&module_name=wings&action_name=delete&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });
    },

  };

});
