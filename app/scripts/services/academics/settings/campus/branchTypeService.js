angular.module('minovateApp')
  .factory('BranchType', function($http, $cookies) {

    return {
      index: function(subdomain, domain) {
        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/branch_types?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=branchtype&action_name=index&in=branchtypes_index&perform_action=branchtypes.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },
      save: function(subdomain, domain, branchData) {
        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/branch_types?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=branchtype&action_name=create&in=branchtypes_create&perform_action=branchtypes.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(branchData)

        });

      },
      patch: function(subdomain, domain, branchData, id) {
        return $http({
          method: 'PUT',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/branch_types/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=branchtype&action_name=create&in=branchtypes_create&perform_action=branchtypes.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(branchData)

        });

      }

    }

  });
