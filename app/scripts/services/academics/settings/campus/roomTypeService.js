angular.module('minovateApp')
  .factory('RoomType', function($http, $cookies) {
    return {
      index: function(subdomain, domain) {
        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/room_types?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=facility&action_name=index&in=room_type_index&perform_action=room_types.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      },
      patch: function(subdomain, domain, roomData, id) {
        return $http({
          method: 'PATCH',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/room_types/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=facility&action_name=update&in=room_type_update&a=' + id + '&perform_action=room_types.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(roomData)

        });
      },
      save: function(subdomain, domain, roomData) {
        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/room_types?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=facility&action_name=create&in=room_type_create&perform_action=room_types.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(roomData)
        });
      }
    }
  });
