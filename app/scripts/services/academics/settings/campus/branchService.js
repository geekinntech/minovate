angular.module('minovateApp')
  .service('Branch', function($http, $cookies) {

    return {
      index: function(subdomain, domain) {
        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/branches?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=branch&action_name=index&in=branch_index' + '&perform_action=index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },

      get: function(subdomain, domain, id) {
        return $http.get('http://' + subdomain + '.' + domain + '/api/branches/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=branch&action_name=show&in=branch_show' + '&a=' + id + '&perform_action=branch.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name);
      },

      save: function(subdomain, domain, branchData) {
        return $http({
          method: 'POST',
          url: 'http://' + subdomain + '.' + domain + '/api/branches?perform_action=' + 'Branch Creation' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=branch_create' + '&module_name=branch&action_name=create',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(branchData)

        });

      },

      patch: function(subdomain, domain, branch_update, id) {

        return $http({
          method: 'PATCH',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/branches/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=branch&action_name=update&in=branch_update' + '&a=' + id + '&perform_action=branch.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(branch_update)

        });

      },


      // destroy a comment
      destroy: function(subdomain, domain, id) {
        return $http({
          method: 'DELETE',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/branches/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=branch&action_name=delete&in=branch_delete&a=' + id + '&perform_action=branch.delete' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });
      },
    }

  });
