angular.module('minovateApp')

.factory('Facility', function($http, $cookies) {

  return {
    index: function(subdomain, domain) {
      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/room_facilities?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=facility&action_name=index&in=facility_index&perform_action=facility.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
    patch: function(subdomain, domain, facilityData, id) {

      return $http({
        method: 'PATCH',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/room_facilities/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=facility&action_name=update&in=facility_update&a=' + id + '&perform_action=room_types.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(facilityData)

      });

    },
    save: function(subdomain, domain, facilityData) {
      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/room_facilities?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=facility&action_name=create&in=facility_create&perform_action=facility.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(facilityData)

      });

    }




  }

});
