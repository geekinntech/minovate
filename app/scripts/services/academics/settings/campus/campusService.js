angular.module('minovateApp')
  .service('Campus', function($http, $cookies) {

    return {
      index: function(subdomain, domain) {

        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/schools?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=school&action_name=index&in=school_index' + '&perform_action=school.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },
      get: function(subdomain, domain, id) {

        return $http.get('http://' + subdomain + '.' + domain + '/api/schools?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=school&action_name=show&in=school_show&a=' + id + '&perform_action=school.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name);

      },


      // save a comment (pass in comment data)
      save: function(subdomain, domain, branchData) {

        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/schools?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=school&action_name=create&in=school_create' + '&perform_action=school.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          data: $.param(branchData),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }


        });

      },
      patch: function(subdomain, domain, branchData, id) {

        return $http({
          method: 'PATCH',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/schools/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=school&action_name=update&in=school_update&a=' + id + '&perform_action=school.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(branchData)

        });

      },


      // destroy a comment
      destroy: function(subdomain, domain, id) {
        return $http({
          method: 'DELETE',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/schools?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=school&action_name=delete&in=school_delete&a=' + id + '&perform_action=school.delete' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });
      },




    }

  });
