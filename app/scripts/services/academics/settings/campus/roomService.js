angular.module('minovateApp')

.factory('ClassRoom', function($http, $cookies) {
  return {

    index: function(subdomain, domain) {

      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/class_rooms?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=rooms&action_name=index&in=rooms_index&perform_action=rooms.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
    get: function(subdomain, domain, id) {
      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/class_rooms/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=rooms&action_name=show&in=rooms_show&a=' + id + '&perform_action=rooms.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },


    save: function(subdomain, domain, classData) {

      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/class_rooms?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=rooms&action_name=create&in=rooms_create&perform_action=rooms.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(classData)

      });

    },
    patch: function(subdomain, domain, classData, id) {

      return $http({
        method: 'PATCH',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/class_rooms/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=rooms&action_name=update&in=rooms_update&a=' + id + '&perform_action=rooms.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(classData)

      });

    },


    destroy: function(subdomain, domain, id) {
      return $http({
        method: 'DELETE',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/class_rooms/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=rooms&action_name=delete&in=rooms_delete&a=' + id + '&perform_action=rooms.delete' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });
    },
  }
});
