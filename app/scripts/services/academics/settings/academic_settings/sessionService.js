angular.module('minovateApp')
  .factory('Session', function($http, $cookies) {

    return {

      index: function(subdomain, domain, wing_id) {

        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/sessions?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=sessions_index&perform_action=sessions.index' + '&module_name=shifts&action_name=index&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },
      get: function(subdomain, domain, id) {
        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/sessions?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=sessions_show&a=' + id + '&perform_action=sessions.show' + '&module_name=shifts&action_name=show&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });

      },


      // save a comment (pass in comment data)
      save: function(subdomain, domain, sessionData) {
        return $http({
          method: 'POST',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/sessions?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=sessions_create&perform_action=sessions.create' + '&module_name=shifts&action_name=create&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(sessionData)

        });

      },
      patch: function(subdomain, domain, sessionData, id) {
        return $http({
          method: 'PATCH',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/sessions/' + id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=sessions_update&a=' + id + '&perform_action=sessions.update' + '&module_name=shifts&action_name=update&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(sessionData)

        });

      },
      destroy: function(subdomain, domain, id) {
        return $http({
          method: 'DELETE',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/sessions?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=sessions_delete&a=' + id + '&perform_action=sessions.delete' + '&module_name=shifts&action_name=delete&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });
      },




    }
  })
