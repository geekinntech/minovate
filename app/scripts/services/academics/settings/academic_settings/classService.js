angular.module('minovateApp')

.factory('Class', function($http, $cookies) {

  return {

    index: function(subdomain, domain, wing_id) {

      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",

        url: 'http://' + subdomain + '.' + domain + '/api/lectures?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=classes&action_name=index&in=classes_index&perform_action=classes.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
    get_classes: function(subdomain, domain, wing_id) {

      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=classes&action_name=index&in=get_classes&perform_action=classes.get_classes' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
    update_sections: function(subdomain, domain, data) {

      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=classes&action_name=index&in=update_sections&perform_action=classes.get_classes' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(data)

      });

    },

    get_class_section: function(subdomain, domain, data) {

      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=classes&action_name=index&in=get_class_section&perform_action=classes.get_classes' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(data)

      });

    },
    get: function(subdomain, domain, id) {
      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=classes&action_name=show&in=classes_show&a=' + id + '&perform_action=classes.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },


    // save a comment (pass in comment data)
    save: function(subdomain, domain, classData) {
      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=classes&action_name=create&in=classes_create&perform_action=classes.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(classData)

      });

    },
    patch: function(subdomain, domain, classData, id) {

      return $http({
        method: 'PATCH',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=classes&action_name=update&in=classes_update&a=' + id + '&perform_action=classes.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(classData)

      });

    },


    // destroy a comment
    destroy: function(subdomain, domain, id) {
      return $http({
        method: 'DELETE',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=classes&action_name=delete&in=classes_delete&a=' + id + '&perform_action=classes.delete' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });
    },




  }
}).factory('Section', function($http, $cookies) {

  return {

    index: function(subdomain, domain, wing_id) {

      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/router?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=sections&action_name=index&in=sections_index&perform_action=sections.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
  }
});
