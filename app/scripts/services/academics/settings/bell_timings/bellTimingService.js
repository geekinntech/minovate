angular.module('minovateApp')

.factory('BellTiming', function($http, $cookies) {

  return {

    index: function(subdomain, domain, wing_id) {

      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/bells?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=bells&action_name=index&in=bells_index&perform_action=bells.index' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },
    get: function(subdomain, domain, id) {
      return $http({
        method: 'GET',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/bells/'+id+'?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=bells&action_name=show&in=bells_show&a=' + id + '&perform_action=bells.show' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });

    },


    save: function(subdomain, domain, bellData) {

      return $http({
        method: 'POST',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/bells?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=bells&action_name=create&in=bells_create&perform_action=bells.create' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(bellData)

      });

    },
    patch: function(subdomain, domain, bellData, id) {

      return $http({
        method: 'PATCH',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/bells/'+id+'?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=bells&action_name=update&in=bells_update&a=' + id + '&perform_action=bells.update' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(bellData)

      });

    },


    // destroy a comment
    destroy: function(subdomain, domain, id) {
      return $http({
        method: 'DELETE',
        crossDomain: true,
        dataType: "JSONP",
        url: 'http://' + subdomain + '.' + domain + '/api/bells/'+id+'?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=bells&action_name=delete&in=bells_delete&a=' + id + '&perform_action=bells.delete' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&branch_id=' + $cookies.branch_id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }

      });
    },




  }
})
