angular.module('minovateApp')

.factory('AuthService', function($http) {

  return {

    login: function(subdomain, domain, data) {
      
      return $http({
        method: 'Post',
        crossDomain: true,
        dataType: "JSONP",
        url: "http://" + subdomain + "." + domain + "/api/users/sign_in",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(data)

      });
    },

    index: function(subdomain, domain, data) {
      return $http({
        method: 'Get',
        crossDomain: true,
        dataType: "JSONP",
        url: "http://" + subdomain + "." + domain + "/router?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=get_all_users&module_name=user&action_name=index",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(data)
      });
    },

    create: function(subdomain, domain, data) {
      return $http({
        method: 'Post',
        crossDomain: true,
        dataType: "JSONP",
        url: "http://" + subdomain + "." + domain + "/router?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=create_user&module_name=user&action_name=create",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(data)
      });
    },

    send_recovery_email: function(subdomain, domain, data) {
      return $http({
        method: 'Post',
        crossDomain: true,
        dataType: "JSONP",
        url: "http://" + subdomain + "." + domain + "/api/members/reset_password",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(data)
      });
    },

    update_password_by_token: function(subdomain, domain, data) {
      return $http({
        method: 'Post',
        crossDomain: true,
        dataType: "JSONP",
        url: "http://" + subdomain + "." + domain + "/api/members/update_password_by_token",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(data)
      });
    },

    update: function(subdomain, domain, data) {
      return $http({
        method: 'PATCH',
        crossDomain: true,
        dataType: "JSONP",
        url: "http://" + subdomain + "." + domain + "/router?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=update_user&a=" + data.id + "&module_name=user&action_name=update",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(data)
      });
    },

    destroy: function(subdomain, domain, data) {
      return $http({
        method: 'DELETE',
        crossDomain: true,
        dataType: "JSONP",
        url: "http://" + subdomain + "." + domain + "/router?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=delete_user&a=" + data.id + "&module_name=user&action_name=delete",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
    },

    update_password: function(subdomain, domain, data, user_id) {

      return $http({
        method: 'PUT',
        crossDomain: true,
        dataType: "JSONP",
        url: "http://" + subdomain + "." + domain + "/router?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=update_password&a=" + user_id + "&module_name=user&action_name=update",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(data)
      });
    },

    get_modules: function(subdomain, domain, data) {
      return $http({
        method: 'Get',
        crossDomain: true,
        dataType: "JSONP",
        url: "http://" + subdomain + "." + domain + "/get_all_modules",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(data)
      });
    },

    logout: function(subdomain, domain, data) {
      return $http({
        method: 'DELETE',
        crossDomain: true,
        dataType: "JSONP",
        url: "http://" + subdomain + "." + domain + "/api/users/sign_out" + "?in=sign_out&module_name=user&action_name=index&user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
    },
  }
});
