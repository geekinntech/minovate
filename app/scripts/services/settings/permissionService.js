angular.module('minovateApp')
  .factory('Permission', function($http, $cookies) {
    return {
      index: function(subdomain, domain, data) {
        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/permissions?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=get_all_permissions&module_name=permission&action_name=index" + '&branch_id=' + $cookies.branch_id + "&perform_action=Get User Permission" + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      },

      get_module_name: function(subdomain, domain, data) {

        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/permission_module_names?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=get_all_permissions&module_name=permission&action_name=index" + '&branch_id=' + $cookies.branch_id + "&perform_action=Get User Permission" + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      },

      get: function(subdomain, domain, id) {

        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/permissions/" + id + "?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=get_all_permissions&module_name=permission&action_name=index" + '&branch_id=' + $cookies.branch_id + "&perform_action=Get User Permission" + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      },

      get_user_permission: function(subdomain, domain) {

        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/permissions/get_user_permission?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=get_all_permissions&module_name=permission&action_name=index" + '&branch_id=' + $cookies.branch_id + "&perform_action=Get User Permission" + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      },

      user_role_permission: function(subdomain, domain) {

        return $http({
          method: 'GET',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/permissions/user_role_permission?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=get_all_permissions&module_name=permission&action_name=index" + '&branch_id=' + $cookies.branch_id + "&perform_action=Get User Permission" + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      },


      destroy: function(subdomain, domain, data) {
        return $http({
          method: 'DELETE',
          crossDomain: true,
          dataType: "JSONP",
          url: 'http://' + subdomain + '.' + domain + '/api/permissions/' + data.id + '?user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=delete_permission&a=' + data.id + '&module_name=permission&action_name=delete' + '&branch_id=' + $cookies.branch_id + "&perform_action=Delete Permission" + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }

        });
      },
      create: function(subdomain, domain, data) {

        return $http({
          method: 'POST',
          crossDomain: true,
          url: "http://" + subdomain + "." + domain + "/api/permissions?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=get_all_permissions&module_name=permission&action_name=index" + '&branch_id=' + $cookies.branch_id + "&perform_action=Get User Permission" + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(data)
        });
      },


      update: function(subdomain, domain, data, updateId) {
        return $http({
          method: 'PUT',
          crossDomain: true,
          url: "http://" + subdomain + "." + domain + "/api/permissions/" + updateId + "?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=get_all_permissions&module_name=permission&action_name=index" + '&branch_id=' + $cookies.branch_id + "&perform_action=Get User Permission" + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(data)
        });
      }
    }
  });
