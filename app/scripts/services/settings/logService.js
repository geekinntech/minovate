angular.module('minovateApp')
  .factory('LogService', function($http, $cookies) {

    return {


      search_logs: function(subdomain, domain, start_date, end_date) {
        return $http({
          method: 'POST',
          crossDomain: true,
          url: 'http://' + subdomain + "." + domain + '/api/logs/get_selected_logs?perform_action=' + 'See Logs History' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&module_name=logging&action_name=index' + '&branch_id=' + $cookies.branch_id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param({
            date_to: start_date,
            date_from: end_date
          })

        });
      },
    }

  });
