angular.module('minovateApp')
  .factory('User', ['$q', '$http', '$cookies', function($q, $http, $cookies) {

    return {

      index: function(subdomain, domain, data) {
        return $http({
          method: 'Get',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/members?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=get_all_users&module_name=user&action_name=index",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(data)
        });
      },

      create: function(subdomain, domain, data) {
        return $http({
          method: 'Post',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/members?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=create_user&module_name=user&action_name=create",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(data)
        });
      },

      update: function(subdomain, domain, data) {
        return $http({
          method: 'PATCH',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/members/" + data.id + "?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=update_user&a=" + data.id + "&module_name=user&action_name=update",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(data)
        });
      },

      destroy: function(subdomain, domain, data) {
        return $http({
          method: 'DELETE',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/members/" + data.id + "?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=delete_user&a=" + data.id + "&module_name=user&action_name=delete",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      },

      update_password: function(subdomain, domain, data) {

        return $http({
          method: 'PUT',
          crossDomain: true,
          dataType: "JSONP",
          url: "http://" + subdomain + "." + domain + "/api/members/" + data.id + "/update_password" + "?user_email=" + $cookies.email + "&user_token=" + $cookies.auth_token + "&in=update_password&a=" + data.id + "&module_name=user&action_name=update",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(data)
        });
      }
    }
  }]);
