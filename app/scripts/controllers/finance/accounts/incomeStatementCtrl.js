'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:FormsCommonCtrl
 * @description
 * # FormsCommonCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')

.controller('IncomeStatementCtrl', ['$scope', '$filter', '$modal', '$location', '$cookies', 'Global',
  '$resource',
  '$mdDialog',
  'Permission',
  'AccountReports',
  'AccountSettings',
  'ngTableParams',
  function($scope, $filter, $modal, $location, $cookies, Global, $resource, $mdDialog, Permission, AccountReports, AccountSettings, ngTableParams) {


    $scope.subdomain = $location.$$host.split('.')[0];
    $scope.domain = Global.rails_domain;
    $scope.reports_domain = Global.reports_domain;
    $scope.permissions = {};
    $scope.permissionToAdd = {};
    $scope.page = {
      title: 'Income Statement',
      // subtitle: ' // list of all the permissions'
    };

    $scope.income_statement = {
      filter: 1
    };
    $scope.load_data = function() {
      $scope.show = false;
      $scope.records = {};
      AccountSettings.get($scope.subdomain, $scope.domain)
        .success(function(data) {
          $scope.account_settings = data.account_settings;
          $scope.fiscal_years = data.fiscal_years;
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          console.clear()
          console.dir(data);

        }).error(function(data) {
          console.dir(data);
          $scope.error_submit();
        });
    }

    $scope.label = function(start, end) {
      return start + "-" + end;
    }

    $scope.load_data();

    function formatDate(ptr_date) {
      var day = ptr_date.split('/')[0];
      var month = ptr_date.split('/')[1];
      var year = ptr_date.split('/')[2];
      var formattedDate = new Date(year, month, day);
      return formattedDate;
    }



    $scope.today = function() {
      $scope.dt = new Date();
    };



    $scope.clear = function() {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open1 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened1 = true;
    };

    $scope.open2 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened2 = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      'class': 'datepicker'
    };

    $scope.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[1];


    $scope.get_income_statement = function() {
      

      /////////////////// Submission //////////////////
      $scope.params = {}
      if ($scope.income_statement.filter == 1 && $scope.income_statement.fiscal_year) {
        $scope.params.start = JSON.parse($scope.income_statement.fiscal_year).start;
        $scope.params.end = JSON.parse($scope.income_statement.fiscal_year).end;
      } else if ($scope.income_statement.filter == 2 && $scope.income_statement.start_date && $scope.income_statement.end_date) {
        $scope.params.start = $scope.income_statement.start_date;
        $scope.params.end = $scope.income_statement.end_date;
      }
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      AccountReports.income_statement($scope.subdomain, $scope.reports_domain, $scope.params)
        .success(function(data) {
          $scope.report = data.report;
          $scope.excel_url = 'http://' + $scope.subdomain + '.' + $scope.reports_domain + data.excel_url;
          $scope.pdf_url = 'http://' + $scope.subdomain + '.' + $scope.reports_domain + data.pdf_url;
          if ($scope.type == 1)
            window.open($scope.pdf_url);
          else if ($scope.type == 2)
            window.open($scope.excel_url);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          console.dir(data);
          $scope.error_submit();
        });
    }

  }
]);
