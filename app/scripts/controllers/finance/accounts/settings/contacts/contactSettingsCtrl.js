'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:FormsCommonCtrl
 * @description
 * # FormsCommonCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')
  .controller('ContactSettingsCtrl', ['$scope', '$filter', '$modal', '$location', '$cookies', 'Global',
    '$resource',
    '$mdDialog',
    'City',
    'ContactSettings',
    'ngTableParams',
    '$rootScope',
    '$timeout',

    function($scope, $filter, $modal, $location, $cookies, Global, $resource, $mdDialog, City, ContactSettings, ngTableParams, $rootScope, $timeout) {

      $scope.subdomain = $location.$$host.split('.')[0];
      $scope.domain = Global.rails_domain;
      $scope.page = {
        title: 'Contact Settings',
        // subtitle: ' // list of all the ContactSettingss'
      };



      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      ////////////////////contact settings index////////////////////////
      ContactSettings.index($scope.subdomain, $scope.domain, [])
        .success(function(data_ret) {
          $scope.data = data_ret.contacts;
          $scope.countries = data_ret.countries;
          $scope.provinces = data_ret.provinces;

          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
              id: 'asc' // initial sorting
            },
            filter: {
              // first_name: 'M' // initial filter
            },
          }, {
            total: $scope.data.length, // length of data
            getData: function($defer, params) {
              // use build-in angular filter
              var filteredData = params.filter() ?
                $filter('filter')($scope.data, params.filter()) :
                $scope.data;
              var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                $scope.data;
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });

        })
        .error(function(data_ret) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          console.dir(data_ret);
          $scope.errorShow("Something went wrong. Try again later.");

        });

      ////////////////////create contact popup/////////////////////////
      $scope.createPopupShow = function(size) {
          $scope.contact = {};
          $scope.cities = {};
          $rootScope.updateChosen();
          $scope.contact.c_type = 1;
          $scope.contact_update = false;
          $scope.contact_view = false;

          $.fancybox.showLoading();
          $.fancybox.helpers.overlay.open({
            parent: $('body')
          });
          $scope.user = {};
          $scope.dialogBox = $modal.open({
            windowClass: 'app-modal-window',
            templateUrl: 'views/tmpl/finance/accounts/settings/contact/dialog/create.html',
            size: size,
            windowClass: 'app-modal-window',
            scope: $scope
          });

          $scope.dialogBox.opened.then(function() {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          });
        }
        //////////////////////// Filters ////////////////////////////////////

      $scope.filter = function(element) {
        return element.selected == true;
      }
      $scope.xfilter = function(element) {
        return element.selected == false;
      }
      $scope.idFilter = function(id, value, index, array) {
        return value.id == id;
      }
      $scope.c_idFilter = function(id, value, index, array) {
        return value.c_id == id;
      }
      $scope.valueFilter = function(id, value, index, array) {
        return value.value == id;
      }

      $scope.codeFilter = function(id, value, index, array) {
        return value.code == id;
      }

      ////////////////////////Update popup////////////////////////////
      $scope.updatePopupShow = function(id, size) {
          $scope.contact_update = true;
          $scope.contact_view = false;
          $scope.contact = jQuery.extend(true, {}, $scope.data.filter($scope.c_idFilter.bind(null, id))[0]);
          $scope.c_type = $scope.contact_types.filter($scope.valueFilter.bind(null, $scope.contact.c_type))[0];
          $scope.get_cities();
          $.fancybox.showLoading();
          $.fancybox.helpers.overlay.open({
            parent: $('body')
          });
          $scope.user = {};
          $scope.dialogBox = $modal.open({
            windowClass: 'app-modal-window',
            templateUrl: 'views/tmpl/finance/accounts/settings/contact/dialog/update.html',
            size: size,
            windowClass: 'app-modal-window',
            scope: $scope
          });

          $scope.dialogBox.opened.then(function() {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          });

        }
        ///////////////////////view popup//////////////////////////
      $scope.viewPopupShow = function(id, size) {
        $scope.contact_view = true;
        $scope.contact = jQuery.extend(true, {}, $scope.data.filter($scope.c_idFilter.bind(null, id))[0]);
        $scope.c_type = $scope.contact_types.filter($scope.valueFilter.bind(null, $scope.contact.c_type))[0];
        $scope.get_cities();
        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        $scope.user = {};
        $scope.dialogBox = $modal.open({
          windowClass: 'app-modal-window',
          templateUrl: 'views/tmpl/finance/accounts/settings/contact/dialog/view.html',
          size: size,
          windowClass: 'app-modal-window',
          scope: $scope
        });

        $scope.dialogBox.opened.then(function() {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        });

      }


      /////////////////////dismiss popup//////////////////
      $scope.dismissIt = function() {
        $scope.dialogBox.dismiss('cancel');
      }

      $scope.contact = {};
      $scope.contact_types = [{
        name: 'Company Contact',
        url: 'views/tmpl/finance/accounts/settings/contact/view/companyContact.html',
        value: 1
      }, {
        name: 'Contact Person',
        url: 'views/tmpl/finance/accounts/settings/contact/view/contactPerson.html',
        value: 2
      }];

      $scope.c_type = $scope.contact_types[0];
      //////////////// check if supplier or customer is updated//////////////////
      $scope.isCategorySelected = function() {
          return ($scope.contact.supplier || $scope.contact.customer);
        }
        //////////////////change contact type////////////////////////
      $scope.change_contact_c_type = function(c_type) {
          $scope.contact.c_type = c_type.value;
        }
        ////////////////create contact///////////////////////////////
      $scope.create_contact_submit = function() {

          $.fancybox.showLoading();
          $.fancybox.helpers.overlay.open({
            parent: $('body')
          });
          ContactSettings.save($scope.subdomain, $scope.domain, $scope.contact)
            .success(function(data) {
              if (data.error === "Contact Name exists") {
                $scope.errorShow("Contact Name exists");
              } else {
                $scope.data = data.contacts;
                $scope.tableParams.total($scope.data.length);
                $scope.tableParams.reload();
                $rootScope.showNotify('Contact successfully created.')
              }
              $.fancybox.hideLoading();
              $.fancybox.helpers.overlay.close();
              $scope.dismissIt();


            }).error(function(data) {
              $.fancybox.hideLoading();
              $.fancybox.helpers.overlay.close();
              console.dir(data);
              $scope.errorShow("Something went wrong. Try again later.");
            });
        }
        ///////////////////update contact////////////////////////
      $scope.update_contact_submit = function() {
        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        ContactSettings.patch($scope.subdomain, $scope.domain, $scope.contact, $scope.contact.id)
          .success(function(data) {
            if (data.error === "Contact Name exists") {
              $scope.errorShow("Contact Name exists");
            } else {
              $scope.data = data.contacts;
              $scope.tableParams.total($scope.data.length);
              $scope.tableParams.reload();
              $rootScope.showNotify('Contact successfully updated.')
            }
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $scope.dismissIt();

          }).error(function(data) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            console.dir(data);
            $scope.errorShow("Something went wrong. Try again later.");

          });
      }

      // $scope.delete_submit = function() {
      //   $.fancybox.showLoading();
      //   $.fancybox.helpers.overlay.open({
      //     parent: $('body')
      //   });

      //   ContactSettings.destroy($scope.subdomain, $scope.domain, $scope.contact, $scope.contact.id)
      //     .success(function(data) {
      //       console.log(data);
      //     }).error(function(data) {
      //       $.fancybox.hideLoading();
      //       $.fancybox.helpers.overlay.close();
      //       console.log(data);
      //       $scope.error_submit();
      //     });
      // }

      /////////////////get cities////////////////////
      $scope.get_cities = function() {
        City.index($scope.subdomain, $scope.domain, $scope.contact.country)
          .success(function(data) {
            $scope.cities = data.cities;
          }).error(function(data) {
            console.dir(data);
          }).then(function() {
            $rootScope.updateChosen();
          });
      }

      // $scope.$watch('cities', function() {
      //   debugger;
      //   $timeout(function() {
      //     element.trigger('chosen:updated');
      //   });
      // });


    }
  ]);
