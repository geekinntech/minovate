'use strict';



angular.module('minovateApp')
  .controller('AccountSettingsCtrl', ['$scope', '$rootScope', '$filter', '$modal', '$location', '$cookies', 'Global',
    '$resource',
    '$mdDialog',
    'Permission',
    'COA',
    'City',
    'Bank',
    'Journal',
    'AccountSettings',
    'ngTableParams',
    function($scope, $rootScope, $filter, $modal, $location, $cookies, Global, $resource, $mdDialog, Permission, COA, City, Bank, Journal, AccountSettings, ngTableParams) {

      $scope.subdomain = $location.$$host.split('.')[0];
      $scope.domain = Global.rails_domain;
      $scope.permissions = {};
      $scope.page = {
        title: 'Account Settings',
      };
      ////////////Date/////////
      $scope.dp = {};
      $scope.today = function() {
        $scope.dt = new Date();
      };
      $scope.clear = function() {
        $scope.dt = null;
      };

      // Disable weekend selection
      $scope.disabled = function(date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
      };

      $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
      };
      $scope.toggleMin();


      ///////////////////////////Account Settings/////////////////////////
      ////////////////////////////////Start///////////////////////////////
      ////////////////////////////////////////////////////////////////////
      $scope.openFiscalStart = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.dp.openedFiscalStart = true;
      };

      $scope.openFiscalEnd = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.dp.openedFiscalEnd = true;
      };

      $scope.openTaxStart = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.dp.openedTaxStart = true;
      };

      $scope.openTaxEnd = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.dp.openedTaxEnd = true;
      };
      $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        minMode: 'month'
      };

      $scope.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate', 'yyyy/MM'];
      $scope.format = $scope.formats[4];

      $scope.format_date = function(date, id) {
        var datefilter = $filter('date'),
          formattedDate = datefilter(date, $scope.formats[id]);
        // alert(formattedDate);
        return formattedDate;
      }
      $scope.set_fiscal_end = function() {
        var d = new Date($scope.account_settings.fiscal_start);
        d.setDate(d.getDate() - 1);
        d.setFullYear(d.getFullYear() + 1);
        $scope.account_settings.fiscal_end = $scope.format_date(d, 4);

      }


      $scope.set_tax_end = function() {
        var d = new Date($scope.account_settings.tax_start);
        d.setDate(d.getDate() - 1);
        d.setFullYear(d.getFullYear() + 1);
        $scope.account_settings.tax_end = $scope.format_date(d, 4);
      }

      $scope.show_settings = function() {
        $rootScope.hideNotify();
      }


      $scope.populateAccountSettingsTable = function() {

        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        AccountSettings.get($scope.subdomain, $scope.domain, 0)
          .success(function(dataRet) {
            $scope.data = dataRet.account_settings;
            $scope.account_settings = dataRet.account_settings;
            var fiscalstart = new Date($scope.account_settings.fiscal_start);
            var taxstart = new Date($scope.account_settings.tax_start);
            $scope.account_settings.fiscal_start = $scope.format_date(fiscalstart, 4);
            $scope.account_settings.tax_start = $scope.format_date(taxstart, 4);
            $scope.all_currencies = dataRet.currencies;
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          })
          .error(function(dataRet) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            console.dir(dataRet);
            $scope.errorShow("Something went wrong. Try again later.");

          });

      }


      $scope.populateAccountSettingsTable();


      $scope.account_settings_submit = function() {
        if ($scope.all_currencies.filter($scope.filter).length == 0) {
          $scope.errorShow("Select atleast 1 currency.");
          return false;
        }
        if ($scope.all_currencies.filter($scope.filter).filter($scope.basefilter).length == 0) {
          $scope.errorShow("Select a base currency.");
          return false;
        }
        if ($scope.all_currencies.filter($scope.filter).filter($scope.basefilter).length > 1) {
          $scope.errorShow("Select only 1 base currency.");
          return false;
        }

        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        $scope.account_settings.currencies = $scope.all_currencies;
        AccountSettings.save($scope.subdomain, $scope.domain, $scope.account_settings)
          .success(function(data) {
            $scope.account_settings = data.account_settings;
            $scope.all_currencies = data.currencies; //.filter($scope.xfilter);
            var fiscalstart = new Date($scope.account_settings.fiscal_start);
            var taxstart = new Date($scope.account_settings.tax_start);
            $scope.account_settings.fiscal_start = $scope.format_date(fiscalstart, 4);
            $scope.account_settings.tax_start = $scope.format_date(taxstart, 4);
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $rootScope.showNotify('Settings updated successfully');
            console.dir(data);
          }).error(function(data) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            console.dir(data);
            $scope.errorShow("Something went wrong. Try again later.");
          });
      }
      $scope.temp = {
        c_id: ""
      };

      $scope.add_currency = function() {
        if ($scope.temp.c_id && $scope.all_currencies.filter($scope.idFilter.bind(null, $scope.temp.c_id))[0]) {
          $scope.all_currencies.filter($scope.idFilter.bind(null, $scope.temp.c_id))[0].selected = true;
          $scope.temp.c_id = "";
          $rootScope.updateChosen();
        } else {
          $scope.errorShow("Select a currency then add.");
        }

      }

      $scope.set_base = function(id) {
        $scope.all_currencies.filter($scope.idFilter.bind(null, id))[0].conversion = 1;
      }

      $scope.remove_currency = function(id) {
          $scope.all_currencies.filter($scope.idFilter.bind(null, id))[0].selected = false;
          $scope.all_currencies.filter($scope.idFilter.bind(null, id))[0].base = false;
        }
        /////////////////////////////////Account Settings End////////////////////////////////////////

      $scope.dismissIt = function() {
        $scope.dialogBox.dismiss('cancel');
      }

      //////////////////////// Filters ////////////////////////////////////

      $scope.filter = function(element) {
        return element.selected == true;
      }
      $scope.leaf_filter = function(element) {
        return element.leaf_level_status == true;
      }
      $scope.xfilter = function(element) {
        return element.selected == false;
      }
      $scope.basefilter = function(element) {
        return element.base == true;
      }
      $scope.parent_filter = function(element) {
        return element.leaf_level_status == false;
      }
      $scope.idFilter = function(id, value, index, array) {
        return value.id == id;
      }

      $scope.coa_idFilter = function(id, value, index, array) {
        return value.coa_id == id;
      }

      $scope.codeFilter = function(code, value, index, array) {
        return value.code === code;
      }

      $scope.titleFilter = function(title, value, index, array) {
        return value.title === title;
      }

      ////////////////////////////////////////////////////////////////
      //////////////////////////// COA   /////////////////////////////
      ////////////////////////////////////////////////////////////////


      $scope.show_coa = function() {
        $rootScope.hideNotify();
        if ($scope.chart_of_accounts && $scope.chart_of_accounts.length > 0) {

        } else {
          $scope.load_accounts();
        }
      }
      $scope.load_accounts = function() {


        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        COA.index($scope.subdomain, $scope.domain)
          .success(function(dataRet) {
            $scope.chart_of_accounts = dataRet.chart_of_accounts;
            $scope.data = dataRet.chart_of_accounts;
            $scope.account_types = dataRet.account_types;
            $scope.parent_accounts = dataRet.parent_accounts;
            $scope.countries = dataRet.countries;
            $scope.provinces = dataRet.provinces;
            $scope.tableParams1 = new ngTableParams({
              page: 1, // show first page
              count: 10, // count per page
              sorting: {
                title: 'asc' // initial sorting
              },
            }, {
              total: $scope.data.length, // length of data
              getData: function($defer, params) {
                // use build-in angular filter
                var filteredData = params.filter() ?
                  $filter('filter')($scope.data, params.filter()) :
                  $scope.data;
                var orderedData = params.sorting() ?
                  $filter('orderBy')(filteredData, params.orderBy()) :
                  $scope.data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
              }
            });
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          })
          .error(function(dataRet) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            console.dir(dataRet);
            $scope.errorShow("Something went wrong. Try again later.");
          });
      }
      $scope.parent;
      $scope.set_code = function() {
        $scope.parent = $scope.parent_accounts.filter($scope.coa_idFilter.bind(null, $scope.coa.parent_account_id))[0];
        $scope.coa.code1 = $scope.parent.code;
        if ($scope.parent.title === "Current Assets" || $scope.parent.title === "Current Asset") {
          $scope.show_bank = true;
        } else
          $scope.show_bank = false;
      }

      $scope.append_code = function(num, level) {
        if (level <= 2)
          var size = 2;
        else
          var size = 3;
        var s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
      }

      $scope.get_cities = function() {
        City.index($scope.subdomain, $scope.domain, $scope.bank.bank_country)
          .success(function(data) {
            $scope.cities = data;
          }).error(function(data) {
            console.dir(data);
          });
      }


      /////////////////////////// Create ///////////////////////////////

      $scope.create_account = function(size) {
        $scope.disable_leaf = false;
        $scope.coa = {};
        $scope.coa.leaf_level_status = false;
        $scope.coa.active_status = false;
        $scope.isError = false;
        if ((parent.title === "Current Assets" || parent.title === "Current Asset") && $scope.coa.bank != undefined) {
          $scope.show_bank = true;
        } else
          $scope.show_bank = false;
        $scope.dialogBox = $modal.open({
          windowClass: 'app-modal-window',
          templateUrl: 'views/tmpl/finance/accounts/settings/coa/dialog/create_coa.html',
          size: size,
          backdrop: 'static',
          windowClass: 'app-modal-window',
          scope: $scope
        });
      }

      $scope.create_coa_submit = function() {
        if ($scope.coa.code == $scope.parent.code) {
          $scope.errorShow("Append your account code");
          return false;
        }
        $scope.coa.code = $scope.coa.code1 + $scope.append_code($scope.coa.code2, $scope.parent.level + 1)
        if ($scope.chart_of_accounts.filter($scope.codeFilter.bind(null, $scope.coa.code)).length > 0) {
          $scope.errorShow("Code alreasy exists");
          return false;
        }
        if ($scope.chart_of_accounts.filter($scope.titleFilter.bind(null, $scope.coa.title)).length > 0) {
          $scope.errorShow("Chart Of Account Title alreasy exists");
          return false;
        }
        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        COA.save($scope.subdomain, $scope.domain, $scope.coa)
          .success(function(data) {
            $scope.chart_of_accounts = data.chart_of_accounts;
            $scope.data = data.chart_of_accounts;
            $scope.account_types = data.account_types;
            $scope.parent_accounts = data.parent_accounts;
            $scope.countries = data.countries;
            $scope.provinces = data.provinces;
            $scope.dismissIt();
            $scope.tableParams1.total($scope.data.length);
            $scope.tableParams1.reload();
            $scope.coa = {};
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $rootScope.showNotify('Charts of account created successfully');
          }).error(function(data) {
            console.dir(data);
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $scope.errorShow("Something went wrong. Try again later.");
          });



      }

      /////////////////////////// Edit ///////////////////////////////

      $scope.edit_accounts = function(id, size) {
        $scope.coa = jQuery.extend(true, {}, $scope.chart_of_accounts.filter($scope.idFilter.bind(null, id))[0]);
        $scope.parent = $scope.parent_accounts.filter($scope.coa_idFilter.bind(null, $scope.coa.parent_account_id))[0];
        $scope.coa.code1 = $scope.coa.parent_account_code;
        if ($scope.coa.code.split($scope.coa.parent_account_code).length == 2)
          $scope.coa.code2 = parseInt($scope.coa.code.split($scope.coa.parent_account_code)[1]);
        else if ($scope.coa.code.split($scope.coa.parent_account_code).length > 2) {
          $scope.coa.code2 = parseInt($scope.coa.parent_account_code + $scope.coa.code.split($scope.coa.parent_account_code)[2]);
        }
        if (($scope.parent.title === "Current Assets" || $scope.parent.title === "Current Asset") && $scope.coa.bank != undefined) {
          $scope.show_bank = true;
        } else
          $scope.show_bank = false;
        $scope.dialogBox = $modal.open({
          windowClass: 'app-modal-window',
          templateUrl: 'views/tmpl/finance/accounts/settings/coa/dialog/update_coa.html',
          size: size,
          backdrop: 'static',
          windowClass: 'app-modal-window',
          scope: $scope
        });
      }

      $scope.edit_coa_submit = function() {
        if ($scope.coa.code == parent.code) {
          $scope.errorShow("Append your account code");
          return false;
        }
        $scope.coa.code = $scope.coa.code1 + $scope.append_code($scope.coa.code2, $scope.parent.level + 1)
        if ($scope.chart_of_accounts.filter($scope.codeFilter.bind(null, $scope.coa.code)).length > 0) {
          if ($scope.chart_of_accounts.filter($scope.codeFilter.bind(null, $scope.coa.code))[0].id != $scope.coa.id) {
            $scope.errorShow("Code alreasy exists");
            return false;
          }
        }

        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        COA.patch($scope.subdomain, $scope.domain, $scope.coa, $scope.coa.id)
          .success(function(data) {
            $scope.chart_of_accounts = data.chart_of_accounts;
            $scope.data = data.chart_of_accounts;
            $scope.account_types = data.account_types;
            $scope.parent_accounts = data.parent_accounts;
            $scope.tableParams1.total($scope.data.length);
            $scope.tableParams1.reload();
            $scope.dismissIt();
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $rootScope.showNotify('Charts of account updated successfully');
          }).error(function(data) {
            console.dir(data);
            $scope.dismissIt();
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $scope.errorShow("Something went wrong. Try again later.");
          });
      }

      /////////////////////////// view ///////////////////////////////

      $scope.view_accounts = function(id, size) {
        $scope.coa = $scope.chart_of_accounts.filter($scope.idFilter.bind(null, id))[0];
        var parent = $scope.parent_accounts.filter($scope.coa_idFilter.bind(null, $scope.coa.parent_account_id))[0];
        if ((parent.title === "Current Assets" || parent.title === "Current Asset") && $scope.coa.bank != undefined) {
          $scope.show_bank = true;
        } else
          $scope.show_bank = false;
        $scope.dialogBox = $modal.open({
          windowClass: 'app-modal-window',
          templateUrl: 'views/tmpl/finance/accounts/settings/coa/dialog/show_coa.html',
          size: size,
          backdrop: 'static',
          windowClass: 'app-modal-window',
          scope: $scope
        });
      }


      ///////////////////////// Add Bank //////////////////////////

      $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
      };

      $scope.add_bank = function(size) {
        $scope.bank = {};
        $scope.account = {};
        $('.btn').blur();

        $scope.dialogBoxBank = $modal.open({
          windowClass: 'app-modal-window',
          templateUrl: 'views/tmpl/finance/accounts/settings/coa/bank/create_bank.html',
          size: size,
          backdrop: 'static',
          windowClass: 'app-modal-window',
          scope: $scope
        });
      }
      $scope.add_bank_submit = function() {
        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        $scope.bank.account = $scope.account;
        Bank.save($scope.subdomain, $scope.domain, $scope.bank)
          .success(function(data) {
            $scope.coa.account_id = data.bank_account.id;
            $scope.coa.leaf_level_status = true;
            $scope.disable_leaf = true;
            $scope.dismissBank();
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          }).error(function(data) {
            console.dir(data);
            $scope.dismissBank();
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $scope.errorShow("Something went wrong. Try again later.");
          })
      }
      $scope.dismissBank = function() {
        $scope.dialogBoxBank.dismiss('cancel');
      }

      ///////////////////////// Edit Bank ////////////////////////////

      $scope.edit_bank = function(size) {
        $scope.bank = $scope.coa.bank;
        $scope.account = $scope.coa.bank_account;
        $('.btn').blur();
        $scope.get_cities();
        $scope.dialogBoxBank = $modal.open({
          windowClass: 'app-modal-window',
          templateUrl: 'views/tmpl/finance/accounts/settings/coa/bank/update_bank.html',
          size: size,
          backdrop: 'static',
          windowClass: 'app-modal-window',
          scope: $scope
        });

      }
      $scope.edit_account_submit = function() {
        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        $scope.bank.account = $scope.account;
        Bank.patch($scope.subdomain, $scope.domain, $scope.bank, $scope.bank.id)
          .success(function(data) {
            $.fancybox.hideLoading();
            $scope.coa.account_id = data.bank_account.id;
            $scope.coa.leaf_level_status = true;
            $scope.disable_leaf = true;
            $scope.dismissBank();
            $.fancybox.helpers.overlay.close();
          }).error(function(data) {
            console.dir(data);
            $scope.error_submit();
          })
      }

      $scope.view_bank = function(size) {
        $scope.bank = $scope.coa.bank;
        $scope.account = $scope.coa.bank_account;
        $('.btn').blur();
        $scope.get_cities();
        $scope.dialogBoxBank = $modal.open({
          windowClass: 'app-modal-window',
          templateUrl: 'views/tmpl/finance/accounts/settings/coa/bank/show_bank.html',
          size: size,
          backdrop: 'static',
          windowClass: 'app-modal-window',
          scope: $scope
        });

      }

      ////////////////////////////////////////////////////////////////
      /////////////////////////// Journal   //////////////////////////
      ////////////////////////////////////////////////////////////////

      $scope.show_journal = function() {
        $rootScope.hideNotify();
        if ($scope.leaf_accounts && $scope.leaf_accounts.length > 0) {

        } else
          $scope.load_journals();
      }
      $scope.load_journals = function() {


          $.fancybox.showLoading();
          $.fancybox.helpers.overlay.open({
            parent: $('body')
          });
          Journal.index($scope.subdomain, $scope.domain)
            .success(function(dataRet) {
              $scope.leaf_accounts = dataRet.chart_of_accounts.filter($scope.leaf_filter);
              $scope.journals = dataRet.journals;
              $scope.data = dataRet.journals;


              $scope.tableParams2 = new ngTableParams({
                page: 1, // show first page
                count: 10, // count per page
                sorting: {
                  name: 'asc' // initial sorting
                },
              }, {
                total: $scope.data.length, // length of data
                // counts: [],
                getData: function($defer, params) {
                  // use build-in angular filter
                  var filteredData = params.filter() ?
                    $filter('filter')($scope.data, params.filter()) :
                    $scope.data;
                  var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.data;
                  $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                  $.fancybox.hideLoading();
                  $.fancybox.helpers.overlay.close();
                }
              })

            }).error(function(dataRet) {
              $.fancybox.hideLoading();
              $.fancybox.helpers.overlay.close();
              console.dir(dataRet);
              $rootScope.errorShow("Something went wrong. Try again later.");
            });

        }
        //////////////////////////// create //////////////////////////////////
      $scope.create_journal = function(size) {
        $scope.journal = {};

        $scope.dialogBox = $modal.open({
          windowClass: 'app-modal-window',
          templateUrl: 'views/tmpl/finance/accounts/settings/journal/dialog/create_journal.html',
          size: size,
          backdrop: 'static',
          windowClass: 'app-modal-window',
          scope: $scope
        });
      }

      $scope.create_journal_submit = function() {
        if ($scope.journals.filter($scope.codeFilter.bind(null, $scope.journal.code)).length > 0) {
          $scope.errorShow("Code alreasy exists");
          return false;
        }
        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        Journal.save($scope.subdomain, $scope.domain, $scope.journal)
          .success(function(data) {
            $scope.leaf_accounts = data.chart_of_accounts.filter($scope.leaf_filter);
            $scope.journals = data.journals;
            $scope.data = data.journals;
            $scope.dismissIt();
            $scope.tableParams2.total($scope.data.length);
            $scope.tableParams2.reload();
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $rootScope.showNotify('Journal created successfully');
          }).error(function(data) {
            console.dir(data);
            $scope.errorShow("Something went wrong. Try again later.");
          });
      }

      //////////////////////////// edit //////////////////////////////////
      $scope.edit_journal = function(id, size) {
        $scope.journal = jQuery.extend(true, {}, $scope.journals.filter($scope.idFilter.bind(null, id))[0]);
        $scope.dialogBox = $modal.open({
          windowClass: 'app-modal-window',
          templateUrl: 'views/tmpl/finance/accounts/settings/journal/dialog/update_journal.html',
          size: size,
          backdrop: 'static',
          windowClass: 'app-modal-window',
          scope: $scope
        });
      }

      $scope.edit_journal_submit = function() {
        if ($scope.journals.filter($scope.codeFilter.bind(null, $scope.journal.code)).length > 0) {
          if ($scope.journals.filter($scope.codeFilter.bind(null, $scope.journal.code))[0].id != $scope.journal.id) {
            $scope.show_alert("Code alreasy exists");
            return false;
          }
        }
        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        Journal.patch($scope.subdomain, $scope.domain, $scope.journal, $scope.journal.id)
          .success(function(data) {
            $scope.leaf_accounts = data.chart_of_accounts.filter($scope.leaf_filter);
            $scope.journals = data.journals;
            $scope.data = data.journals;
            $scope.dismissIt();
            $scope.tableParams2.total($scope.data.length);
            $scope.tableParams2.reload();
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $rootScope.showNotify('Journal updated successfully');
          }).error(function(data) {
            console.dir(data);
            $scope.error_submit();
          });
      }

      //////////////////////////// view //////////////////////////////////
      $scope.view_journal = function(id, size) {
        $scope.journal = $scope.journals.filter($scope.idFilter.bind(null, id))[0];
        $scope.dialogBox = $modal.open({
          windowClass: 'app-modal-window',
          templateUrl: 'views/tmpl/finance/accounts/settings/journal/dialog/show_journal.html',
          size: size,
          backdrop: 'static',
          windowClass: 'app-modal-window',
          scope: $scope
        });
      }
    }

  ]);
