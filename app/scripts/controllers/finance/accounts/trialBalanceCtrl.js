'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:FormsCommonCtrl
 * @description
 * # FormsCommonCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')

.controller('TrialBalanceCtrl', ['$scope', '$filter', '$modal', '$location', '$cookies', 'Global',
  '$resource',
  '$mdDialog',
  'Permission',
  'AccountReports',
  'AccountSettings',
  'ngTableParams',
  function($scope, $filter, $modal, $location, $cookies, Global, $resource, $mdDialog, Permission, AccountReports, AccountSettings, ngTableParams) {


    $scope.subdomain = $location.$$host.split('.')[0];
    $scope.domain = Global.rails_domain;
    $scope.reports_domain = Global.reports_domain;
    $scope.permissions = {};
    $scope.permissionToAdd = {};
    $scope.page = {
      title: 'Trial Balance',
      // subtitle: ' // list of all the permissions'
    };

    $scope.trial_balance = {};


    $scope.today = function() {
      $scope.dt = new Date();
    };



    $scope.clear = function() {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open1 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened1 = true;
    };


    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      'class': 'datepicker'
    };

    $scope.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[1];
    console.clear();

    $scope.get_trial_balance = function() {


      /////////////////// Submission //////////////////

      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      AccountReports.trial_balance($scope.subdomain, $scope.reports_domain, $scope.trial_balance)
        .success(function(data) {
          $scope.report = data.report;
          $scope.excel_url = 'http://' + $scope.subdomain + '.' + $scope.reports_domain + data.excel_url;
          $scope.pdf_url = 'http://' + $scope.subdomain + '.' + $scope.reports_domain + data.pdf_url;
          if ($scope.type == 1)
            window.open($scope.pdf_url);
          else if ($scope.type == 2)
            window.open($scope.excel_url);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          console.dir(data);
          $scope.error_submit();
        });
    }

  }
]);
