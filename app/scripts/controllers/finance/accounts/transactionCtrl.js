'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:MailCtrl
 * @description
 * # MailCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')

.controller('AccountsTransactionCtrl', ['$scope', '$filter', '$modal', '$location', '$cookies', 'Global',
  '$resource',
  '$mdDialog',
  'ngTableParams',
  '$rootScope',
  'Transaction',
  'City',
  'COA',
  'ContactSettings',
  '$q',
  '$timeout',
  'FileUploader',
  function($scope, $filter, $modal, $location, $cookies, Global, $resource, $mdDialog, ngTableParams, $rootScope, Transaction, City, COA, ContactSettings, $q, $timeout, FileUploader) {


    $scope.subdomain = $location.$$host.split('.')[0];
    $scope.domain = Global.rails_domain;
    $scope.permissions = {};
    $scope.permissionToAdd = {};
    $scope.isError = true;
    $scope.isSuccess = true;
    $scope.page = {
      title: 'Transaction',
      //subtitle: ' // list of all the permissions'
    };
    $scope.allowed = true;
    ///////////////////////////// Filters //////////////////////////////////

    $scope.idFilter = function(id, value, index, array) {
      return value.id == id;
    }

    $scope.cashFilter = function(element) {
      return element.journal_type === "Cash";
    }

    $scope.bankFilter = function(element) {
      return element.journal_type === "Banks";
    }

    $scope.tidFilter = function(id, value, index, array) {
      return value.txn_id == id;
    }

    $scope.parent_Filter = function(id, value, index, array) {
      return value.parent_account_id == id;
    }
    $scope.debit_Filter = function(id, value, index, array) {

      return id.transaction_type == "Debit";
    }
    $scope.credit_Filter = function(id, value, index, array) {
      return id.transaction_type == "Credit";
    }

    // var uploader = $scope.uploader = new FileUploader({
    //   url: $scope.url//
    // });

    //////////////////////////////////////////////////////////////////

    $scope.data = [];
    $scope.currentPage = 1;
    $scope.tableParams1 = new ngTableParams({
      page: 1, // show first page
      count: 100, // count per page

      sorting: {
        id: 'desc' // initial sorting
      },
    }, {
      counts: [],
      total: 0, // length of data
      getData: function($defer, params) {
        // use build-in angular filter
        Transaction.index($scope.subdomain, $scope.domain, $scope.tabs.type, params.page())
          .success(function(data) {
            $scope.cash_receipts = data.cash_receipts;
            $scope.chart_of_accounts = data.chart_of_accounts;
            $scope.cash_accounts = data.cash_accounts;
            $scope.bank_accounts = data.bank_accounts;
            $scope.coas = $scope.chart_of_accounts;
            $scope.journals = data.journals;

            $scope.data = $scope.cash_receipts.filter($scope.debit_Filter);
            var filteredData = params.filter() ?
              $filter('filter')($scope.data, params.filter()) :
              $scope.data;
            var orderedData = params.sorting() ?
              $filter('orderBy')(filteredData, params.orderBy()) :
              $scope.data;
            $defer.resolve(orderedData);
            params.total(data.total);

            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          }).error(function(data) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            console.dir(data);
            $rootScope.errorShow("Error occur while loading data.");
          });
      }
    });

    $scope.tableParams2 = new ngTableParams({
      page: 1, // show first page
      count: 100, // count per page

      sorting: {
        id: 'desc' // initial sorting
      },
    }, {
      counts: [],
      total: 0, // length of data
      getData: function($defer, params) {
        // use build-in angular filter
        Transaction.index($scope.subdomain, $scope.domain, $scope.tabs.type, params.page())
          .success(function(data) {
            $scope.cash_payments = data.cash_payments;
            $scope.chart_of_accounts = data.chart_of_accounts;
            $scope.cash_accounts = data.cash_accounts;
            $scope.bank_accounts = data.bank_accounts;
            $scope.coas = $scope.chart_of_accounts;
            $scope.journals = data.journals;

            $scope.data = $scope.cash_payments.filter($scope.credit_Filter);
            var filteredData = params.filter() ?
              $filter('filter')($scope.data, params.filter()) :
              $scope.data;
            var orderedData = params.sorting() ?
              $filter('orderBy')(filteredData, params.orderBy()) :
              $scope.data;
            $defer.resolve(orderedData);
            params.total(data.total);

            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          }).error(function(data) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            console.dir(data);
            $rootScope.errorShow("Error occur while loading data.");
          });
      }
    });

    $scope.tableParams3 = new ngTableParams({
      page: 1, // show first page
      count: 100, // count per page

      sorting: {
        id: 'desc' // initial sorting
      },
    }, {
      counts: [],
      total: 0, // length of data
      getData: function($defer, params) {

        Transaction.index($scope.subdomain, $scope.domain, $scope.tabs.type, params.page())
          .success(function(data) {
            $scope.manual_journals = data.manual_journals;
            $scope.chart_of_accounts = data.chart_of_accounts;
            $scope.cash_accounts = data.cash_accounts;
            $scope.bank_accounts = data.bank_accounts;
            $scope.coas = $scope.chart_of_accounts;
            $scope.journals = data.journals;

            $scope.data = $scope.manual_journals;
            var filteredData = params.filter() ?
              $filter('filter')($scope.data, params.filter()) :
              $scope.data;
            var orderedData = params.sorting() ?
              $filter('orderBy')(filteredData, params.orderBy()) :
              $scope.data;
            $defer.resolve(orderedData);
            params.total(data.total);

            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          }).error(function(data) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            console.dir(data);
            $rootScope.errorShow("Error occur while loading data.");
          });

      }
    });

    $scope.tableParams4 = new ngTableParams({
      page: 1, // show first page
      count: 100, // count per page

      sorting: {
        id: 'desc' // initial sorting
      },
    }, {
      counts: [],
      total: 0, // length of data
      getData: function($defer, params) {
        Transaction.index($scope.subdomain, $scope.domain, $scope.tabs.type, params.page())
          .success(function(data) {
            $scope.bank_receipts = data.bank_receipts;
            $scope.chart_of_accounts = data.chart_of_accounts;
            $scope.cash_accounts = data.cash_accounts;
            $scope.bank_accounts = data.bank_accounts;
            $scope.coas = $scope.chart_of_accounts;
            $scope.journals = data.journals;

            $scope.data = $scope.bank_receipts.filter($scope.debit_Filter);
            var filteredData = params.filter() ?
              $filter('filter')($scope.data, params.filter()) :
              $scope.data;
            var orderedData = params.sorting() ?
              $filter('orderBy')(filteredData, params.orderBy()) :
              $scope.data;
            $defer.resolve(orderedData);
            params.total(data.total);

            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          }).error(function(data) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            console.dir(data);
            $rootScope.errorShow("Error occur while loading data.");
          });
      }
    });

    $scope.tableParams5 = new ngTableParams({
      page: 1, // show first page
      count: 100, // count per page

      sorting: {
        id: 'desc' // initial sorting
      },
    }, {
      counts: [],
      total: 0, // length of data
      getData: function($defer, params) {
        Transaction.index($scope.subdomain, $scope.domain, $scope.tabs.type, params.page())
          .success(function(data) {
            $scope.bank_payments = data.bank_payments;
            $scope.chart_of_accounts = data.chart_of_accounts;
            $scope.cash_accounts = data.cash_accounts;
            $scope.bank_accounts = data.bank_accounts;
            $scope.coas = $scope.chart_of_accounts;
            $scope.journals = data.journals;

            $scope.data = $scope.bank_payments.filter($scope.credit_Filter);
            var filteredData = params.filter() ?
              $filter('filter')($scope.data, params.filter()) :
              $scope.data;
            var orderedData = params.sorting() ?
              $filter('orderBy')(filteredData, params.orderBy()) :
              $scope.data;
            $defer.resolve(orderedData);
            params.total(data.total);

            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          }).error(function(data) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            console.dir(data);
            $rootScope.errorShow("Error occur while loading data.");
          });
      }
    });


    $scope.load_data = function(type, page) {

      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.tabs = {}
      $scope.tabs.type = type;
      Transaction.index($scope.subdomain, $scope.domain, $scope.tabs.type, page)
        .success(function(data) {
          $scope.currentPage = page;
          $scope.total = data.total;
          $scope.update_tables(data, $scope.tabs.type);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          console.dir(data);
          $rootScope.errorShow("Error occur while loading data.");
        });


    }
    $scope.load_data(1, 1);

    $scope.dismissIt = function() {
      $scope.dialogBox.dismiss('cancel');
    }


    $scope.view_txn = function(txn_id, type, size) {
      type = type - 1;
      if (type == 0) {
        $scope.txns = $scope.cash_receipts.filter($scope.tidFilter.bind(null, txn_id))
      } else if (type == 1) {
        $scope.txns = $scope.cash_payments.filter($scope.tidFilter.bind(null, txn_id))
      } else if (type == 2) {
        $scope.txns = $scope.manual_journals.filter($scope.tidFilter.bind(null, txn_id))
      } else if (type == 3) {
        $scope.txns = $scope.bank_receipts.filter($scope.tidFilter.bind(null, txn_id))
      } else if (type == 4) {
        $scope.txns = $scope.bank_payments.filter($scope.tidFilter.bind(null, txn_id))
      }

      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.user = {};
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/finance/accounts/transaction/dialogs/txn_view.html',
        size: size,
        windowClass: 'app-modal-window-xl',
        scope: $scope
      });

      $scope.dialogBox.opened.then(function() {
        $('#ex1_value').attr("required", "true");
        $('#ex1_value').attr("required", "true");
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
      });
    }
    $scope.accountChange = accountChange;
    $scope.accountTextChange = accountSearch;

    function accountSearch(query) {
      COA.ajax($scope.subdomain, $scope.domain, query).success(function(data) {
        $scope.results = data.chart_of_accounts;
      }).error(function(data) {
        $scope.results = [];

      });

    }


    function accountChange(item) {
      if (item) {
        $scope.transaction.cash_coa_id = item.originalObject.coa_id;
        console.log(JSON.stringify($scope.transaction));
      }
    }


    $scope.contactTextChange = contactSearch;

    function contactSearch(query, index) {
      var contact_type;
      if (index !== undefined) {
        contact_type = $scope.transactions[index].contact_type;
      } else {
        contact_type = $scope.transaction.contact_type;
      }
      ContactSettings.ajax($scope.subdomain, $scope.domain, query, contact_type).success(function(data) {
        $scope.results_contact = data.contacts;
      }).error(function(data) {
        $scope.results = [];
      });

    }

    $scope.setIndex = function(index) {
      $scope.index = index;
    }


    $scope.contactChange = function(item) {
      if (item !== undefined) {
        $scope.transaction.contact_id = item.originalObject.id;
      }
    }

    $scope.contactChangeMj = function(item) {
      if (item && $scope.index >= 0) {
        $scope.transactions[$scope.index].contact_id = item.originalObject.id;
      }
      console.log(JSON.stringify($scope.transactions[$scope.index]));
    }

    $scope.querySearch = function(query) {
      var results = query ? $scope.results_contact.filter(createFilterFor(query)) : $scope.results_contact,
        deferred;
      deferred = $q.defer();
      $timeout(function() {
        deferred.resolve(results);
      }, Math.random() * 1000, false);
      return deferred.promise;

    }


    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(contact) {
        return (angular.lowercase(contact.name).indexOf(lowercaseQuery) === 0);
      };
    }

    $scope.cash_types = [{
      value: "cash",
      name: "Cash"
    }, {
      value: "promissory",
      name: "Promissory Note"
    }];

    $scope.bank_types = [{
      value: "cross_cheque",
      name: "Cross Cheque"
    }, {
      value: "cash_cheque",
      name: "Cash Cheque"
    }, {
      value: "demand_draft",
      name: "Demand Draft"
    }, {
      value: "pay_order",
      name: "Pay Order"
    }, {
      value: "other",
      name: "Other"
    }];




    $scope.create_txn = function(type, tab_name) {
      $scope.tab_name = tab_name;
      $scope.isError = false;
      $scope.disable = false;
      $scope.transaction = {};
      $scope.transaction.t_type = type;
      delete $scope.contact;
      if ($scope.transaction.t_type === 'cr' || $scope.transaction.t_type === 'cp') {
        $scope.filtered_journals = $scope.journals.filter($scope.cashFilter);
        $scope.filtered_coas = $scope.cash_accounts;
        $scope.p_types = $scope.cash_types;
      } else if ($scope.transaction.t_type === 'br' || $scope.transaction.t_type === 'bp') {
        $scope.filtered_journals = $scope.journals.filter($scope.bankFilter);
        $scope.filtered_coas = $scope.bank_accounts;
        $scope.p_types = $scope.bank_types;
      }

      if ($scope.transaction.t_type === 'cr' || $scope.transaction.t_type === 'br') {
        $scope.transaction.transaction_type = "Debit";
        $scope.transaction.x_transaction_type = "Credit";
        $scope.transaction.contact_type = "customer";
      } else if ($scope.transaction.t_type === 'cp' || $scope.transaction.t_type === 'bp') {
        $scope.transaction.transaction_type = "Credit";
        $scope.transaction.x_transaction_type = "Debit";
        $scope.transaction.contact_type = "supplier";
      }

      $scope.dialogBox = $modal.open({
        templateUrl: 'views/tmpl/finance/accounts/transaction/dialogs/create_txn.html',
        size: 'lg',
        backdrop: 'static',
        windowClass: 'app-modal-window-xl',
        scope: $scope
      });
    }

    $scope.create_mj = function(type) {

        $scope.openedTDate = false;
        $scope.isError = false;
        $scope.disable_coa = []
        $scope.disable_coa[0] = false;
        $scope.transactions = [];
        $scope.transactions[0] = {
          journal_id: 0,
          contactChange: function(item) {
            if (item !== undefined) {
              this.contact_id = item.originalObject.id;
            }
          }
        };

        $scope.uploaders = [];
        $scope.uploaders[0] = new FileUploader();


        $scope.disable_leaf = false;
        $scope.mj = {};
        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        $scope.user = {};
        $scope.dialogBox = $modal.open({
          templateUrl: 'views/tmpl/finance/accounts/transaction/dialogs/create_txn_mj.html',
          size: 'lg',
          windowClass: 'app-modal-window-xl',
          scope: $scope
        });

        $scope.dialogBox.opened.then(function() {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        });
      }
      /////////////////////////////// MJ ////////////////////////////////////
    $scope.status = "Unbalanced"
    $scope.calc_cr_dr = function(index) {
      $scope.cr_amount = 0;
      $scope.dr_amount = 0;
      for (var i = 0; i < $scope.transactions.length; i++) {
        if ($scope.transactions[i].transaction_type === "Debit") {
          // $scope.transactions[i].contact_type = "customer";
          $scope.dr_amount = $scope.dr_amount + parseInt($scope.transactions[i].amount);
        } else if ($scope.transactions[i].transaction_type === "Credit") {
          // $scope.transactions[i].contact_type = "supplier";
          $scope.cr_amount = $scope.cr_amount + parseInt($scope.transactions[i].amount);
        }
      }
      if ($scope.cr_amount != $scope.dr_amount) {
        $scope.status = "Unbalanced"
      } else
        $scope.status = "Balanced"
      $scope.set_manual_journal(index);
    }

    $scope.inc_count = function() {
      $scope.transactions[$scope.transactions.length] = {
        journal_id: 0
      };

      $scope.uploaders[$scope.uploaders.length] = new FileUploader();

    }

    $scope.delete_row = function(index) {
      delete $scope.transactions[index];
      delete $scope.disable_coa[index];
      delete $scope.uploaders[index];
      $scope.transactions.clean(undefined);
      $scope.disable_coa.clean(undefined);
      $scope.uploaders.clean(undefined);
    }
    $scope.resetContact = function(index) {

      $scope.$broadcast('angucomplete-alt:clearInput', 'ex' + index);

    }

    $scope.required = true;

    $scope.set_manual_journal = function(index) {
      if ($scope.transactions[index].journal_id == 0) {
        $scope.disable_coa[index] = false;
      } else {
        var journal = $scope.journals.filter($scope.idFilter.bind(null, $scope.transactions[index].journal_id))[0];
        if ($scope.transactions[index].transaction_type === "Debit" && journal.debit_account_id) {
          $scope.allowed = true;

          $scope.filtered_coas = $scope.chart_of_accounts;
          $scope.transactions[index].chart_of_account_id = $scope.journals.filter($scope.idFilter.bind(null, $scope.transactions[index].journal_id))[0].debit_account.coa_id;
          $scope.disable_coa[index] = true;
        } else if ($scope.transactions[index].transaction_type === "Credit" && journal.credit_account_id) {
          $scope.allowed = true;
          $scope.filtered_coas = $scope.chart_of_accounts;
          $scope.transactions[index].chart_of_account_id = $scope.journals.filter($scope.idFilter.bind(null, $scope.transactions[index].journal_id))[0].credit_account.coa_id;
          $scope.disable_coa[index] = true;
        } else {
          $scope.allowed = false;
          $scope.disable_coa[index] = false;
          if (parseInt($scope.transactions[index].journal_id) != 0) {
            $scope.transactions[index].chart_of_account_id = "";
            if ($scope.transactions[index].transaction_type === "Debit" && !journal.debit_account_id) {
              $rootScope.errorShow("No Debit Account selected for this journal");
            } else if ($scope.transactions[index].transaction_type === "Credit" && !journal.credit_account_id) {
              $rootScope.errorShow("No Credit Account selected for this journal");
            }
          }
        }

      }
    }


    $scope.set_journal = function(index) {
      if ($scope.transaction.journal_id == 0) {
        $scope.disable = false;
        $scope.transaction.chart_of_account_id = "";
      } else {
        var journal = $scope.journals.filter($scope.idFilter.bind(null, $scope.transaction.journal_id))[0];
        if ($scope.transaction.transaction_type === "Debit" && journal.debit_account_id) {
          $scope.allowed = true;

          $scope.filtered_coas = $scope.chart_of_accounts;
          $scope.transaction.chart_of_account_id = $scope.journals.filter($scope.idFilter.bind(null, $scope.transaction.journal_id))[0].debit_account.coa_id;
          $scope.disable = true;
        } else if ($scope.transaction.transaction_type === "Credit" && journal.credit_account_id) {
          $scope.allowed = true;
          $scope.filtered_coas = $scope.chart_of_accounts;
          $scope.transaction.chart_of_account_id = $scope.journals.filter($scope.idFilter.bind(null, $scope.transaction.journal_id))[0].credit_account.coa_id;
          $scope.disable = true;
        } else {
          $scope.allowed = false;
          $scope.disable = false;
          if (parseInt($scope.transaction.journal_id) != 0) {
            $scope.transaction.chart_of_account_id = "";
            if ($scope.transaction.transaction_type === "Debit" && !journal.debit_account_id) {
              $rootScope.errorShow("No Debit Account selected for this journal");
            } else if ($scope.transaction.transaction_type === "Credit" && !journal.credit_account_id) {
              $rootScope.errorShow("No Credit Account selected for this journal");
            }
          }
        }

      }
    }

    $scope.uploader = new FileUploader({
      url: "abc"
    });


    $scope.create_txn_submit = function() {

      if (!$scope.allowed) {
        $rootScope.errorShow("Select Debit/Credit Account for selected Journal from Account Settings");
        return false;
      }
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      Transaction.save($scope.subdomain, $scope.domain, $scope.transaction)
        .success(function(data) {
          $scope.id = data.id;
          if ($scope.uploader.queue.length > 0) {
            $scope.uploader.queue[0].url = 'http://' + $scope.subdomain + '.' + $scope.domain + '/api/transactions/upload_file/?id=' + $scope.id + '&user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&branch_id=' + $cookies.branch_id + '&page=' + 1;

            $scope.uploader.uploadAll();
          } else {
            $scope.total = data.total;
            $scope.update_tables(data, $scope.tabs.type);
          }
        }).error(function(data) {
          console.dir(data);
          $rootScope.errorShow("Something went wrong");
        });

    }

    $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
      $scope.total = response.total;
      $scope.update_tables(response, $scope.tabs.type);
      console.info('onCompleteitem');
    };



    $scope.update_tables = function(response, type) {
      $scope.cash_receipts = response.cash_receipts;
      $scope.cash_payments = response.cash_payments;
      $scope.bank_receipts = response.bank_receipts;
      $scope.bank_payments = response.bank_payments;
      $scope.manual_journals = response.manual_journals;
      $scope.chart_of_accounts = response.chart_of_accounts;
      $scope.cash_accounts = response.cash_accounts;
      $scope.bank_accounts = response.bank_accounts;
      $scope.coas = $scope.chart_of_accounts;
      $scope.journals = response.journals;

      if (type == 1) {
        $scope.data = $scope.cash_receipts.filter($scope.debit_Filter);
        $scope.tableParams1.total($scope.total);
        $scope.tableParams1.reload();

      } else if (type == 2) {
        $scope.data = $scope.cash_payments.filter($scope.credit_Filter);
        console.log($scope.data);
        $scope.tableParams2.total($scope.total);
        $scope.tableParams2.reload();

      } else if (type == 3) {
        $scope.data = $scope.manual_journals;
        $scope.tableParams3.total($scope.total);
        $scope.tableParams3.reload();

      } else if (type == 4) {
        $scope.data = $scope.bank_receipts.filter($scope.debit_Filter);
        $scope.tableParams4.total($scope.total);
        $scope.tableParams4.reload();

      } else if (type == 5) {
        $scope.data = $scope.bank_payments.filter($scope.credit_Filter);
        $scope.tableParams5.total($scope.total);
        $scope.tableParams5.reload();

      }
      $scope.dismissIt();
      $.fancybox.hideLoading();
      $.fancybox.helpers.overlay.close();

    }

    $scope.dismissIt = function() {
      if ($scope.dialogBox)
        $scope.dialogBox.dismiss('cancel');
    }

    $scope.today = function() {
      $scope.dt = new Date();
    };



    $scope.clear = function() {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open1 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened1 = true;
    };
    $scope.openTDate = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.openedTDate = $scope.openedTDate ? false : true;
    };


    $scope.open2 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened2 = true;
    };

    $scope.postingDate = function($event, index) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.transactions[index].postingDateValue = $scope.transactions[index].postingDateValue ? false : true;
    };


    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      'class': 'datepicker'
    };

    $scope.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[1];


    //create manual journal submit
    $scope.create_mj_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });

      for (var i = 0; i < $scope.transactions.length; i++) {
        if ($scope.transactions[i].contact)
          $scope.transactions[i].contact_id = $scope.transactions[i].contact.originalObject.id;
      }

      $scope.mj.transactions = $scope.transactions;
      $scope.mj.type = "mj";

      Transaction.save($scope.subdomain, $scope.domain, $scope.mj)
        .success(function(data) {
          $scope.ids = data.ids;
          $scope.uploader.queue = [];
          var count = 0;
          for (var i = 0; i < $scope.ids.length; i++) {


            if ($scope.uploaders[i].queue.length > 0) {
              $scope.uploader.queue[count] = $scope.uploaders[i].queue[0];
              $scope.uploader.queue[count].url = 'http://' + $scope.subdomain + '.' + $scope.domain + '/api/transactions/upload_file/?id=' + $scope.ids[i] + '&user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&branch_id=' + $cookies.branch_id;
              $scope.uploader.queue[count].upload();
              $scope.uploader.queue[count].onComplete = function(response, status, headers) {
                $scope.total = response.total;
                $scope.update_tables(response, $scope.tabs.type);
                console.info('onComplete');
              };
              count++;

            }

          }
          if (count == 0)
            $scope.load_data($scope.tabs.type);
          $scope.dismissIt();
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          console.dir(data);
          $scope.errorShow('Error occur while creating manual journal.');
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        });
    }


    ///////////////////////// create Contact /////////////////////////////////

    $scope.add_contact = function() {
      $('.btn').blur();
      $scope.contact = {};
      $scope.hide = true;
      $scope.contact.c_type = 1;
      $scope.contact_types = [{
        name: 'Company Contact',
        url: 'views/tmpl/finance/accounts/settings/contact/view/companyContact.html',
        value: 1
      }, {
        name: 'Contact Person',
        url: 'views/tmpl/finance/accounts/settings/contact/view/contactPerson.html',
        value: 2
      }];
      $scope.dialogBoxContact = $modal.open({
        templateUrl: 'views/tmpl/finance/accounts/settings/contact/dialog/create2.html?bust=' + Math.random().toString(36).slice(2),
        size: 'md',
        windowClass: 'app-modal-window',
        scope: $scope
      });
    }

    $scope.isCategorySelected = function() {
      return ($scope.contact.supplier || $scope.contact.customer);
    }

    $scope.create_contact_submit = function() {

      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      ContactSettings.save($scope.subdomain, $scope.domain, $scope.contact)
        .success(function(data) {
          if (data.error === "Contact Name exists") {
            $scope.errorShow("Contact Name exists");
          } else {
            //$scope.contacts = data.contacts;
            //$rootScope.showNotify('contact successfully created')
          }
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.dismissContact();


        }).error(function(data) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          console.dir(data);
          $rootScope.errorShow("Something went wrong");
        });
    }

    $scope.dismissContact = function() {
      if ($scope.dialogBoxContact)
        $scope.dialogBoxContact.dismiss('cancel');
    }

  }
]);

//'http://scitech.scitech.managewise.dev:3000/api/transactions/upload_file/?id=' + 333 + '&user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token //enable this option to get f
