'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:FormsCommonCtrl
 * @description
 * # FormsCommonCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')

.controller('LedgersCtrl', ['$scope', '$filter', '$modal', '$location', '$cookies', 'Global',
  '$resource',
  '$mdDialog',
  'Permission',
  'AccountReports',
  'AccountSettings',
  'COA',
  'ContactSettings',
  'ngTableParams',
  '$q',
  '$timeout',
  function($scope, $filter, $modal, $location, $cookies, Global, $resource, $mdDialog, Permission, AccountReports, AccountSettings, COA, ContactSettings, ngTableParams, $q, $timeout) {


    $scope.subdomain = $location.$$host.split('.')[0];
    $scope.domain = Global.rails_domain;
    $scope.reports_domain = Global.reports_domain;
    $scope.permissions = {};
    $scope.permissionToAdd = {};
    $scope.page = {
      title: 'Ledgers',
      // subtitle: ' // list of all the permissions'
    };

    $scope.account_ledger = {
      filter: 1
    }
    $scope.contact_ledger = {
      filter: 1
    };


    $scope.load_data = function() {
      $scope.show = false;
      $scope.records = {};
      AccountSettings.get($scope.subdomain, $scope.domain)
        .success(function(data) {
          $scope.chart_of_accounts = data.chart_of_accounts;
          $scope.fiscal_years = data.fiscal_years;
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          //console.clear()
          console.dir(data);

        }).error(function(data) {
          console.dir(data);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong");
        });
    }

    $scope.label = function(start, end) {
      return start + "-" + end;
    }

    $scope.load_data();

    function formatDate(ptr_date) {
      var day = ptr_date.split('/')[0];
      var month = ptr_date.split('/')[1];
      var year = ptr_date.split('/')[2];
      var formattedDate = new Date(year, month, day);
      return formattedDate;
    }


    $scope.accountChange = accountChange;
    $scope.accountTextChange = accountSearch;

    function accountSearch(query) {
      if (query)
        COA.ajax($scope.subdomain, $scope.domain, query).success(function(data) {
          $scope.results = data.chart_of_accounts;
        }).error(function(data) {
          $scope.results = [];

        });

    }


    function accountChange(item) {
      if (item) {
        $scope.account_ledger.coa_id = item.id;
        console.log(JSON.stringify($scope.account_ledger));
      }
    }

    $scope.contactChange = contactChange;
    $scope.contactTextChange = contactSearch;

    function contactSearch(query) {
      if (query && $scope.contact_ledger.type)
        ContactSettings.ajax($scope.subdomain, $scope.domain, query, $scope.contact_ledger.type).success(function(data) {
          $scope.results_contact = data.contacts;
        }).error(function(data) {
          $scope.results = [];
        });

    }


    function contactChange(item) {
      if (item) {
        $scope.contact_ledger.contact_id = item.id;
        console.log(JSON.stringify($scope.contact_ledger));
      }
    }

    $scope.querySearch = function(query) {
      var results = query ? $scope.results_contact.filter(createFilterFor(query)) : $scope.results_contact,
        deferred;
      deferred = $q.defer();
      $timeout(function() {
        deferred.resolve(results);
      }, Math.random() * 1000, false);
      return deferred.promise;

    }


    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(contact) {
        return (angular.lowercase(contact.name).indexOf(lowercaseQuery) === 0);
      };
    }

    $scope.reset_query = function() {
      delete $scope.results_contact;
      delete $scope.contact;
      delete $scope.contact_ledger.contact_id;
    }





    $scope.today = function() {
      $scope.dt = new Date();
    };



    $scope.clear = function() {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open1 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened1 = true;
    };

    $scope.open2 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened2 = true;
    };


    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      'class': 'datepicker'
    };

    $scope.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[1];
    console.clear();

    $scope.get_account_ledger = function() {


      /////////////////// Submission //////////////////

      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      if ($scope.account_ledger.filter == 1) {
        $scope.account_ledger.from = JSON.parse($scope.account_ledger.fiscal_year).start;
        $scope.account_ledger.to = JSON.parse($scope.account_ledger.fiscal_year).end;
      }
      if ($scope.account_ledger.filter == 2) {
        $scope.account_ledger.from = $scope.account_ledger.start
        $scope.account_ledger.to = $scope.account_ledger.end
      }
      AccountReports.account_ledger($scope.subdomain, $scope.reports_domain, $scope.account_ledger)
        .success(function(data) {
          $scope.report = data.report;
          $scope.excel_url = 'http://' + $scope.subdomain + '.' + $scope.reports_domain + data.excel_url;
          $scope.pdf_url = 'http://' + $scope.subdomain + '.' + $scope.reports_domain + data.pdf_url;
          if ($scope.type == 1)
            window.open($scope.pdf_url);
          else if ($scope.type == 2)
            window.open($scope.excel_url);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          console.dir(data);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong");
        });
    }

    $scope.get_contact_ledger = function() {


      /////////////////// Submission //////////////////

      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      if ($scope.contact_ledger.filter == 1) {
        $scope.contact_ledger.from = JSON.parse($scope.contact_ledger.fiscal_year).start;
        $scope.contact_ledger.to = JSON.parse($scope.contact_ledger.fiscal_year).end;
      }
      if ($scope.contact_ledger.filter == 2) {
        $scope.contact_ledger.from = $scope.contact_ledger.start
        $scope.contact_ledger.to = $scope.contact_ledger.end
      }
      AccountReports.contact_ledger($scope.subdomain, $scope.reports_domain, $scope.contact_ledger)
        .success(function(data) {
          $scope.report = data.report;
          $scope.excel_url = 'http://' + $scope.subdomain + '.' + $scope.reports_domain + data.excel_url;
          $scope.pdf_url = 'http://' + $scope.subdomain + '.' + $scope.reports_domain + data.pdf_url;
          if ($scope.type == 1)
            window.open($scope.pdf_url);
          else if ($scope.type == 2)
            window.open($scope.excel_url);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          console.dir(data);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong");
        });
    }

  }
]);
