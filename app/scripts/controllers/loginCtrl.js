'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:PagesLoginCtrl
 * @description
 * # PagesLoginCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')

.controller('LoginCtrl', function($scope, $state, $location, $cookies, Global, AuthService, $mdDialog, $window) {

  $scope.subdomain = $location.$$host.split('.')[0];
  $scope.domain = Global.rails_domain;


  $scope.login = function() {
    $scope.data = {};
    $scope.data.user = $scope.user;
    AuthService.login($scope.subdomain, $scope.domain, $scope.data).success(function(data) {

      $cookies.email = data.email;
      $cookies.auth_token = data.auth_token;
      $cookies.first_name = data.first_name;
      $cookies.last_name = data.last_name;
      $cookies.log_flag = data.log_flag;
      $cookies.role_name = data.role_name;
      $cookies.permission_allowed = JSON.stringify(data.permission_allowed);
      $window.sessionStorage["user_permisison"] = JSON.stringify(data.permission);

      if (data.branch_id != null) {

        $cookies.branch_id = data.branch_id;
      };

      $location.path('/app/dashboard');
    }).error(function(data) {
      $scope.show_alert("Invalid Credentials");


    });

  };


  $scope.show_alert = function(string) {
    $('.btn').blur();

    $mdDialog.show(
      $mdDialog.alert()
      .parent(angular.element(document.querySelector('#popupContainer')))
      .clickOutsideToClose(true)
      .title(string)
      .ariaLabel('Alert')
      .ok('OK')
    );

  }
})

.controller('LogoutCtrl', function($scope, $state, $location, $cookies, Global, AuthService, $mdDialog) {

  delete $cookies.email;
  delete $cookies.auth_token;
  delete $cookies.first_name;
  delete $cookies.last_name;
  delete $cookies.log_flag;
  delete $cookies.role_name;
  delete $cookies.permission_allowed;
  if ($cookies.branch_id)
    delete $cookies.branch_id;

  $state.go("core.login");

});
