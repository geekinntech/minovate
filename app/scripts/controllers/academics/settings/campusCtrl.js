'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:MailCtrl
 * @description
 * # MailCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')

.controller('CampusCtrl', ['$scope', '$filter', '$modal', '$location', '$cookies', 'Global',
  '$resource',
  '$mdDialog',
  'ngTableParams',
  '$rootScope',
  'Transaction',
  'City',
  '$q',
  'Class',
  '$timeout',
  'FileUploader',
  'Branch',
  'Wing',
  'ClassRoom',
  'BranchType',
  'RoomType',
  'Facility',
  'Campus',
  '$document',

  function($scope, $filter, $modal, $location, $cookies, Global, $resource, $mdDialog, ngTableParams, $rootScope, Transaction, City, $q, Class, $timeout, FileUploader, Branch, Wing, ClassRoom, BranchType, RoomType, Facility, Campus, $document) {

    $scope.subdomain = $location.$$host.split('.')[0];
    $scope.domain = Global.rails_domain;
    $scope.isError = true;
    $scope.isSuccess = true;
    $scope.page = {
      title: 'Campus',
    };
    $scope.selectedcity = {};
    $scope.selectedcitybranch = {};

    ///////////////////////////////Date Functions///////////////////

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
    };
    $scope.dp = {};
    $scope.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate', 'yyyy/MM'];
    $scope.format = $scope.formats[1];
    $scope.format_date = function(date, id) {
      var datefilter = $filter('date'),
        formattedDate = datefilter(date, $scope.formats[id]);
      return formattedDate;
    }
    $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.clear = function() {
      $scope.dt = null;
    };
    $scope.disabled = function(date, mode) {
      return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };
    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    ////////////////////////////Filters/////////////////////////////

    $scope.filter = function(element) {
      return element.selected == true;
    }
    $scope.idFilter = function(id, value, index, array) {
      return value.id == id;
    }
    $scope.branchFilter = function(branch_type, value, index, array) {
      return value.branch_type == branch_type;
    }
    $scope.wingFilter = function(wing_title, value, index, array) {
      return value.wing_title == wing_title;
    }
    $scope.subwingFilter = function(subwing_title, value, index, array) {
      return value.subwing_title == subwing_title;
    }
    $scope.wingIdFilter = function(wing_id, value, index, array) {
      return value.wing_id == wing_id;
    }
    $scope.idFilter = function(id, value, index, array) {
      return value.id == id;
    }
    $scope.branchIdFilter = function(branch_id, value, index, array) {
      return value.branch_id == branch_id;
    }

    ////////////////////////////////////////////////////////////////
    //////////////////////////////Wing start//////////////////////
    ////////////////////////////////////////////////////////////////

    $scope.getWings = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.wings = {};
      Wing.index($scope.subdomain, $scope.domain)
        .success(function(data) {
          $scope.wings = data.wings;
          $scope.wings.subwings = data.wings.sub_wings;
          $scope.wing_branches = data.branches;
          for (var j = 0; j < $scope.wings.length; j++) {
            $scope.wings[j].sub_wing_name = "";
            for (var i = 0; i < $scope.wings[j].sub_wings.length; i++) {
              if (i == $scope.wings[j].sub_wings.length - 1) {
                $scope.wings[j].sub_wing_name += $scope.wings[j].sub_wings[i].name;
              } else {
                $scope.wings[j].sub_wing_name += $scope.wings[j].sub_wings[i].name + ", ";
              }
            }
          }
          $scope.data = $scope.wings;
          $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
              id: 'asc' // initial sorting
            },
            filter: {
              // first_name: 'M' // initial filter
            },
          }, {
            total: $scope.data.length, // length of data
            getData: function($defer, params) {
              // use build-in angular filter
              var filteredData = params.filter() ?
                $filter('filter')($scope.data, params.filter()) :
                $scope.data;
              var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                $scope.data;
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          $scope.errorShow("Something went wrong. Try again later.");
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          console.dir(data);
        });
    }
    $scope.init_subwing = function() {
      if (!$scope.wing.sub_wings) {
        $scope.count = 1;
        $scope.wing.sub_wings = new Array();
        for (var i = 0; i < $scope.count; i++) {
          $scope.wing.sub_wings[i] = {
            name: "",
            description: ""
          }
        };
      } else
        $scope.count = $scope.wing.sub_wings.length;
    }
    $scope.inc_count = function() {
      $scope.wing.sub_wings[$scope.count] = {
        name: "",
        description: ""
      }
      $scope.count++;
    }
    $scope.delete_row = function(index) {
      delete $scope.wing.sub_wings[index];
      $scope.count--;
      $scope.wing.sub_wings.clean(undefined);
    }
    $scope.add_wing_popup_show = function(size) {
      $scope.subWingAccordian = true;
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.wing = {};
      if ($scope.wing_branches.filter($scope.filter).length > 0)
        $scope.wing_branches.filter($scope.filter)[0].selected = false;
      $scope.wing_branches[0].selected = true;
      $scope.isError = false;
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/campus/wings/dialog/create.html',
        size: size,
        windowClass: 'app-modal-window',
        scope: $scope
      });
      $scope.dialogBox.opened.then(function() {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
      });
    }
    $scope.update_wing_popup_show = function(id) {
      $scope.subWingAccordian = true;
      if ($scope.wing_branches.filter($scope.filter).length > 0)
        $scope.wing_branches.filter($scope.filter)[0].selected = false;
      $scope.wing = jQuery.extend(true, {}, $scope.wings.filter($scope.idFilter.bind(null, id))[0]);
      $scope.wing_branches.filter($scope.idFilter.bind(null, $scope.wing.branch_id))[0].selected = true;
      $scope.isError = false;
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/campus/wings/dialog/update.html',
        size: 'lg',
        windowClass: 'app-modal-window',
        scope: $scope
      });

      $scope.dialogBox.opened.then(function() {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
      });
    }
    $scope.add_wing_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.add_wing = {};
      $scope.add_wing.name = $scope.wing.name;
      if ($scope.selected_branches == undefined) {
        $scope.add_wing.branch = $scope.wing_branches[0].id;
      } else {
        $scope.add_wing.branch = $scope.selected_branches[0].id;
      }
      if ($scope.wing.sub_wings == undefined) {
        $scope.add_wing.sub_wing = null;
      } else {
        $scope.add_wing.sub_wing = $scope.wing.sub_wings;
      }
      Wing.save($scope.subdomain, $scope.domain, $scope.add_wing)
        .success(function(data) {
          $scope.dismissIt();
          $scope.wings = data.wings;
          $scope.wing_branches = data.branches;
          for (var j = 0; j < $scope.wings.length; j++) {
            $scope.wings[j].sub_wing_name = "";
            for (var i = 0; i < $scope.wings[j].sub_wings.length; i++) {
              if (i == $scope.wings[j].sub_wings.length - 1) {
                $scope.wings[j].sub_wing_name += $scope.wings[j].sub_wings[i].name;
              } else {
                $scope.wings[j].sub_wing_name += $scope.wings[j].sub_wings[i].name + ", ";
              }
            }
          }
          $scope.data = $scope.wings;
          $scope.tableParams.total($scope.data.length);
          $scope.tableParams.reload();
          $rootScope.showNotify('Wing successfully created.')
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          $scope.errorShow("Something went wrong. Try again later.");
          console.dir(data);
        });
    }
    $scope.branch_select = function(index) {
      if (index >= 0) {
        if ($scope.wing_branches.filter($scope.filter).length > 0)
          $scope.wing_branches.filter($scope.filter)[0].selected = false;
        $scope.wing_branches[index].selected = !$scope.wing_branches[index].selected
        $scope.selected_branches = $scope.wing_branches.filter($scope.filter);
      }

    }

    $scope.dismissIt = function() {
      $scope.dialogBox.dismiss('cancel');
    }
    $scope.view_wing = function(id) {
      $scope.subWingAccordian = true;
      if ($scope.wing_branches.filter($scope.filter).length > 0)
        $scope.wing_branches.filter($scope.filter)[0].selected = false;
      $scope.wing = jQuery.extend(true, {}, $scope.wings.filter($scope.idFilter.bind(null, id))[0]);
      $scope.wing_branches.filter($scope.idFilter.bind(null, $scope.wing.branch_id))[0].selected = true;
      $scope.isError = false;
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/campus/wings/dialog/show.html',
        size: 'lg',
        windowClass: 'app-modal-window',
        scope: $scope
      });
      $scope.dialogBox.opened.then(function() {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
      });
    }
    $scope.update_wing = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.add_wing = {};
      $scope.add_wing.name = $scope.wing.name;
      if ($scope.selected_branches == undefined) {
        $scope.add_wing.branch = $scope.wing.branch_id;
      } else {
        $scope.add_wing.branch = $scope.selected_branches[0].id;
      }
      if ($scope.wing.sub_wings == undefined) {
        $scope.add_wing.sub_wing = null;
      } else {
        $scope.add_wing.sub_wing = $scope.wing.sub_wings;
      }
      Wing.patch($scope.subdomain, $scope.domain, $scope.add_wing, $scope.wing.id)
        .success(function(data) {
          $scope.dismissIt();
          $scope.wings = data.wings;
          $scope.wing_branches = data.branches;
          for (var j = 0; j < $scope.wings.length; j++) {
            $scope.wings[j].sub_wing_name = "";
            for (var i = 0; i < $scope.wings[j].sub_wings.length; i++) {
              if (i == $scope.wings[j].sub_wings.length - 1) {
                $scope.wings[j].sub_wing_name += $scope.wings[j].sub_wings[i].name;
              } else {
                $scope.wings[j].sub_wing_name += $scope.wings[j].sub_wings[i].name + ", ";
              }
            }
          }
          $scope.data = $scope.wings;
          $scope.tableParams.total($scope.data.length);
          $scope.tableParams.reload();
          $rootScope.showNotify('Wing successfully Updated.')
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          $scope.errorShow("Something went wrong. Try again later.");
          console.dir(data);
        });
    }

    ////////////////////////////////////////////////////////////////
    //////////////////////////////School start//////////////////////
    ////////////////////////////////////////////////////////////////

    $scope.openFoundingDate = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.dp.openedFoundingDate = true;
    };
    $scope.show_school = function() {
      $rootScope.hideNotify();
      if ($scope.school) {

      } else {
        $scope.load_school();
      }
    }
    $scope.load_school = function() {

      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      Campus.index($scope.subdomain, $scope.domain).success(function(dataRet) {

        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        $scope.school = dataRet.school;
        $scope.countries = dataRet.countries;
        $scope.provinces = dataRet.provinces;
        if ($scope.school == undefined || dataRet.school_logo_url == null || dataRet.school_logo_url == "") {
          $scope.campus_logo_url = 'http://' + $scope.subdomain + '.' + $scope.domain + "/assets/No_image.png";
        } else {
          $scope.campus_logo_url = 'http://' + $scope.subdomain + '.' + $scope.domain + dataRet.school_logo_url;
          if ($scope.school.zip_code != null)
            $scope.school.zip_code = parseInt($scope.school.zip_code);
        }
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
      }).then(function() {
        $scope.get_cities();
        $rootScope.updateChosen();
      });
    }
    $scope.profileUploader = new FileUploader({
      queueLimit: 1,
      removeAfterUpload: true,
      url: "abc"
    });

    $scope.profileUploader.onAfterAddingFile = function(fileItem) {
      console.info('onAfterAddingFile', fileItem);
      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("logoUpload").files[0]);
      oFReader.onload = function(oFREvent) {
        document.getElementById("logoPreview").src = oFREvent.target.result;
      };
    };


    $scope.profileUploader.onSuccessItem = function(fileItem, response, status, headers) {
      console.info('onSuccessItem', fileItem, response, status, headers);
      $.fancybox.hideLoading();
      $.fancybox.helpers.overlay.close();
      $rootScope.showNotify('School successfully updated.')
    };

    $scope.get_cities = function() {
      $scope.selectedcity.name = "";
      City.index($scope.subdomain, $scope.domain, $scope.school.country)
        .success(function(data) {
          $scope.cities = data.cities;
        }).error(function(data) {
          console.dir(data);
        }).then(function() {
          $scope.selectedcity.name = $scope.school.city;
          $rootScope.updateChosen();
        });
    }



    $scope.add_school_submit = function() {
      $scope.school.city = $scope.selectedcity.name;
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      if ($scope.school == undefined || $scope.school.id == undefined) {
        Campus.save($scope.subdomain, $scope.domain, $scope.school).success(function(dataRet) {
          $scope.school.id = dataRet.school.id;
          $scope.branch = {};
          $scope.branch.id = dataRet.branch.id;
          if ($scope.profileUploader.queue.length > 0) {
            $scope.profileUploader.queue[0].url = 'http://' + $scope.subdomain + '.' + $scope.domain + '/api/schools/school_logo?perform_action=' + 'Branch Creation' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=school_create' + '&module_name=school&action_name=create' + '&id=' + $scope.school.id;
            $scope.profileUploader.uploadAll();
          } else {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $rootScope.showNotify('School successfully updated.')
          }
        }).error(function(dataRet, status, headers) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong. Try again later.");
        });
      } else {
        Campus.patch($scope.subdomain, $scope.domain, $scope.school, $scope.school.id).success(function(dataRet) {
          if ($scope.profileUploader.queue.length > 0) {
            $scope.profileUploader.queue[0].url = 'http://' + $scope.subdomain + '.' + $scope.domain + '/api/schools/school_logo?perform_action=' + 'Branch Creation' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=school_create' + '&module_name=school&action_name=create' + '&id=' + $scope.school.id;
            $scope.profileUploader.uploadAll();
          } else {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $rootScope.showNotify('School successfully updated.')
          }
        }).error(function(data, status, headers) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong. Try again later.");
        });
      }
    }

    ////////////////////////////////////////////////////////////////
    //////////////////////////////Branch start//////////////////////
    ////////////////////////////////////////////////////////////////


    $scope.show_branch = function() {
      $rootScope.hideNotify();
      if ($scope.branches && $scope.branches.length > 0) {

      } else {
        $scope.load_branches();
      }
    }
    $scope.load_branches = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      Branch.index($scope.subdomain, $scope.domain)
        .success(function(data) {
          $scope.data = data.branches;
          $scope.branches = data.branches;
          $scope.branch_types = data.branch_types;
          $scope.countries = data.countries;
          $scope.currencies = data.currencies;
          for (var i = 0; i < data.branches.length; i++) {
            if (data.branches[i].branch_zip_code != null)
              data.branches[i].branch_zip_code = parseInt(data.branches[i].branch_zip_code);
            if ($scope.branches[i].branch_type != null)
              $scope.branches[i].branch_type = $scope.branch_types.filter($scope.branchFilter.bind(null, $scope.branches[i].branch_type))[0].branch_type;
            $scope.branches[i].branch_logo_url = 'http://' + $scope.subdomain + '.' + $scope.domain + $scope.branches[i].branch_logo_url
          }
          $scope.tableParamsBranch = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
              branch_title: 'asc' // initial sorting
            },
          }, {
            total: $scope.data.length, // length of data
            getData: function($defer, params) {
              // use build-in angular filter
              var filteredData = params.filter() ?
                $filter('filter')($scope.data, params.filter()) :
                $scope.data;
              var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                $scope.data;
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(dataRet) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          console.dir(dataRet);
          $scope.errorShow("Something went wrong. Try again later.");
        });
    }

    $scope.profileUploaderBranch = new FileUploader({
      queueLimit: 1,
      removeAfterUpload: true,
      url: "abc"
    });

    $scope.profileUploaderBranch.onAfterAddingFile = function(fileItem) {
      console.info('onAfterAddingFile', fileItem);
      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("logoUploadBranch").files[0]);
      oFReader.onload = function(oFREvent) {
        document.getElementById("logoPreviewBranch").src = oFREvent.target.result;
      };
    };


    $scope.profileUploaderBranch.onSuccessItem = function(fileItem, response, status, headers) {
      console.info('onSuccessItem', fileItem, response, status, headers);
      $scope.data = response.branches;
      $scope.branches = response.branches;
      for (var i = 0; i < $scope.branches.length; i++) {
        if ($scope.branches[i].branch_zip_code != null)
          $scope.branches[i].branch_zip_code = parseInt($scope.branches[i].branch_zip_code);
        if ($scope.branches[i].branch_type != null)
          $scope.branches[i].branch_type = $scope.branch_types.filter($scope.branchFilter.bind(null, $scope.branches[i].branch_type))[0].branch_type;
        $scope.branches[i].branch_logo_url = 'http://' + $scope.subdomain + '.' + $scope.domain + $scope.branches[i].branch_logo_url
      }
      $scope.tableParamsBranch.total($scope.data.length);
      $scope.tableParamsBranch.reload();
      $scope.dismissIt();
      $.fancybox.hideLoading();
      $.fancybox.helpers.overlay.close();
    };

    $scope.get_cities_branch = function() {
      $scope.selectedcitybranch.name = "";
      City.index($scope.subdomain, $scope.domain, $scope.branch.branch_country)
        .success(function(data) {
          $scope.cities = data.cities;
        }).error(function(data) {
          console.dir(data);
        }).then(function() {
          $scope.selectedcitybranch.name = $scope.branch.branch_city;
          $rootScope.updateChosen();
        });
    }

    $scope.create_branch = function(size) {
      $scope.branch = {};
      $scope.cities = {};
      $scope.selectedcitybranch.name = "";
      $scope.branch.branch_logo_url = 'http://' + $scope.subdomain + '.' + $scope.domain + "/assets/No_image.png";
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/campus/branch/dialog/create.html',
        size: size,
        backdrop: 'static',
        windowClass: 'app-modal-window',
        scope: $scope
      });
    }

    $scope.create_branch_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.branch.branch_city = $scope.selectedcitybranch.name;
      Branch.save($scope.subdomain, $scope.domain, $scope.branch).success(function(dataRet) {
        if ($scope.profileUploaderBranch.queue.length > 0) {
          $scope.profileUploaderBranch.queue[0].url = 'http://' + $scope.subdomain + '.' + $scope.domain + '/api/branches/branch_logo?perform_action=' + 'Branch Creation' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=branch_create' + '&module_name=branch&action_name=create' + '&id=' + dataRet.branch.id;
          $scope.profileUploaderBranch.uploadAll();
        } else {
          $scope.data = dataRet.branches;
          $scope.branches = dataRet.branches;
          for (var i = 0; i < $scope.branches.length; i++) {
            if ($scope.branches[i].branch_zip_code != null)
              $scope.branches[i].branch_zip_code = parseInt($scope.branches[i].branch_zip_code);
            if ($scope.branches[i].branch_type != null)
              $scope.branches[i].branch_type = $scope.branch_types.filter($scope.branchFilter.bind(null, $scope.branches[i].branch_type))[0].branch_type;
            $scope.branches[i].branch_logo_url = 'http://' + $scope.subdomain + '.' + $scope.domain + $scope.branches[i].branch_logo_url
          }
          $scope.tableParamsBranch.total($scope.data.length);
          $scope.tableParamsBranch.reload();
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $rootScope.showNotify('Branch successfully created.')
          $scope.dismissIt();
        }
      }).error(function(data, status, headers) {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
        $scope.errorShow("Something went wrong. Try again later.");
      });
    }

    $scope.view_branch = function(id) {
      $scope.branch = $scope.branches.filter($scope.idFilter.bind(null, id))[0];
      $scope.get_cities_branch();
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/campus/branch/dialog/show.html',
        size: 'lg',
        backdrop: 'static',
        windowClass: 'app-modal-window',
        scope: $scope
      });
    }
    $scope.edit_branch = function(id) {
      $scope.branch = $scope.branches.filter($scope.idFilter.bind(null, id))[0];
      $scope.get_cities_branch();
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/campus/branch/dialog/update.html',
        size: 'lg',
        backdrop: 'static',
        windowClass: 'app-modal-window',
        scope: $scope
      });
    }
    $scope.edit_branch_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.branch.branch_city = $scope.selectedcitybranch.name;
      Branch.patch($scope.subdomain, $scope.domain, $scope.branch, $scope.branch.id).success(function(dataRet) {
        if ($scope.profileUploaderBranch.queue.length > 0) {
          $scope.profileUploaderBranch.queue[0].url = 'http://' + $scope.subdomain + '.' + $scope.domain + '/api/branches/branch_logo?perform_action=' + 'Branch Creation' + '&role_name=' + $cookies.role_name + '&full_name=' + $cookies.first_name + '.' + $cookies.last_name + '&user_email=' + $cookies.email + '&user_token=' + $cookies.auth_token + '&in=branch_create' + '&module_name=branch&action_name=create' + '&id=' + $scope.branch.id;
          $scope.profileUploaderBranch.uploadAll();
        } else {
          $scope.data = dataRet.branches;
          $scope.branches = dataRet.branches;
          for (var i = 0; i < $scope.branches.length; i++) {
            if ($scope.branches[i].branch_zip_code != null)
              $scope.branches[i].branch_zip_code = parseInt($scope.branches[i].branch_zip_code);
            if ($scope.branches[i].branch_type != null)
              $scope.branches[i].branch_type = $scope.branch_types.filter($scope.branchFilter.bind(null, $scope.branches[i].branch_type))[0].branch_type;
            $scope.branches[i].branch_logo_url = 'http://' + $scope.subdomain + '.' + $scope.domain + $scope.branches[i].branch_logo_url
          }
          $scope.tableParamsBranch.total($scope.data.length);
          $scope.tableParamsBranch.reload();
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $rootScope.showNotify('Branch successfully updated.')
          $scope.dismissIt();
        }
      }).error(function(data, status, headers) {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
        $scope.errorShow("Something went wrong. Try again later.");
      });
    }

    //////////////////////////////// Branch Type  ////////////////////////////

    $scope.create_branch_type = function() {
      $scope.branch_type = {};
      $scope.dialogBoxCreate = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/campus/branch/branch_type/create.html',
        size: 'md',
        backdrop: 'static',
        scope: $scope
      });

    }
    $scope.show_branch_types = function() {
      $scope.dialogBoxShow = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/campus/branch/branch_type/index.html',
        size: 'md',
        backdrop: 'static',
        scope: $scope
      });

    }
    $scope.edit_branch_type = function(id) {
      $scope.branch_type = $scope.branch_types.filter($scope.idFilter.bind(null, id))[0];
      $scope.dialogBoxEdit = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/campus/branch/branch_type/edit.html',
        size: 'md',
        backdrop: 'static',
        scope: $scope
      });

    }

    $scope.dismissBranchCreate = function() {
      $scope.dialogBoxCreate.dismiss('cancel');
    }
    $scope.dismissBranchShow = function() {
      $scope.dialogBoxShow.dismiss('cancel');
    }
    $scope.dismissBranchEdit = function() {
      $scope.dialogBoxEdit.dismiss('cancel');
    }

    $scope.edit_branch_type_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      BranchType.patch($scope.subdomain, $scope.domain, $scope.branch_type, $scope.branch_type.id)
        .success(function(data) {
          $scope.branch_types = data.branch_types;
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong. Try again later.");
        }).then(function() {
          $rootScope.updateChosen();
        });
      $scope.dismissBranchEdit();
    }


    $scope.create_branch_type_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      BranchType.save($scope.subdomain, $scope.domain, $scope.branch_type)
        .success(function(data) {
          $scope.branch_types = data.branch_types;
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong. Try again later.");
        }).then(function() {
          $rootScope.updateChosen();
        });
      $scope.dismissBranchCreate();
    }

    ////////////////////////////////////////////////////////////////
    //////////////////////////////Room start//////////////////////
    ////////////////////////////////////////////////////////////////

    $scope.getClassRooms = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.rooms = {};

      ClassRoom.index($scope.subdomain, $scope.domain)
        .success(function(data) {

          $scope.class_rooms = data.class_rooms;
          $scope.class_room_branches = data.branches;
          $scope.class_room_wings = data.wings;
          $scope.class_room_types = data.room_types;
          $scope.class_room_facilities = data.room_facilities;

          $scope.data = $scope.class_rooms;

          $scope.tableParams2 = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
              id: 'asc' // initial sorting
            },
            filter: {
              // first_name: 'M' // initial filter
            },
          }, {
            total: $scope.data.length, // length of data
            getData: function($defer, params) {
              // use build-in angular filter
              var filteredData = params.filter() ?
                $filter('filter')($scope.data, params.filter()) :
                $scope.data;
              var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                $scope.data;
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });

          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();

        }).error(function(data) {
          console.dir(data);

        });

    }

    $scope.create_room = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });

      delete $scope.room_subwings;
      delete $scope.class_room_wing;
      delete $scope.class_rooms_new;
      delete $scope.selected_class_room_facilities;

      $scope.new_room = {};
      $scope.room_subwings = [];
      $scope.class_room_wing = [];
      $rootScope.updateChosen();
      $scope.isError = false;
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/campus/room/dialog/create.html',
        size: 'lg',
        backdrop: 'static',
        windowClass: 'app-modal-window',
        scope: $scope
      });

      $scope.dialogBox.opened.then(function() {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
      });

    }

    $scope.edit_room = function(id) {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });

      delete $scope.room_subwings;
      delete $scope.class_room_wing;
      delete $scope.class_rooms_new;
      delete $scope.selected_class_room_facilities;

      $scope.room_subwings = [];
      $scope.class_room_wing = [];
      $scope.edit_faci = {};
      $scope.edit_faci.facilities = [];

      $scope.class_new = jQuery.extend(true, {}, $scope.class_rooms.filter($scope.idFilter.bind(null, id))[0]);
      for (var i = 0; i < $scope.class_new.facilities.length; i++) {
        $scope.edit_faci.facilities.push($scope.class_room_facilities.filter($scope.idFilter.bind(null, $scope.class_new.facilities[i].id))[0]);
      }
      $scope.room_branch_select($scope.class_new.branch_id);
      $rootScope.updateChosen();
      $scope.room_wing_select($scope.class_new.wing_id);
      $rootScope.updateChosen();
      $scope.room_subwing_select($scope.class_new.sub_wing_id);
      $rootScope.updateChosen();
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/campus/room/dialog/update.html',
        size: 'lg',
        backdrop: 'static',
        windowClass: 'app-modal-window',
        scope: $scope
      });

      $scope.dialogBox.opened.then(function() {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
      });

    }
    $scope.show_room = function(id) {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });


      delete $scope.room_subwings;
      delete $scope.class_room_wing;
      delete $scope.class_rooms_new;
      delete $scope.selected_class_room_facilities;

      $scope.room_subwings = [];
      $scope.class_room_wing = [];
      $scope.selected_class_room_facilities = [];

      $scope.room = jQuery.extend(true, {}, $scope.class_rooms.filter($scope.idFilter.bind(null, id))[0]);
      for (var i = 0; i < $scope.room.facilities.length; i++) {
        $scope.selected_class_room_facilities.push($scope.class_room_facilities.filter($scope.idFilter.bind(null, $scope.room.facilities[i].id))[0]);
      }
      $scope.room_branch_select($scope.room.branch_id);
      $rootScope.updateChosen();
      $scope.room_wing_select($scope.room.wing_id);
      $rootScope.updateChosen();
      $scope.room_subwing_select($scope.room.sub_wing_id);
      $rootScope.updateChosen();
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/campus/room/dialog/show.html',
        size: 'lg',
        backdrop: 'static',
        windowClass: 'app-modal-window',
        scope: $scope
      });

      $scope.dialogBox.opened.then(function() {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
      });

    }

    $scope.room_branch_select = function(branch_id) {
      if (branch_id > 0) {
        if ($scope.class_room_branches.filter($scope.idFilter.bind(null, branch_id)).length > 0) {
          $scope.selected_branch = $scope.class_room_branches.filter($scope.idFilter.bind(null, branch_id))[0];
        }
      }
      delete $scope.class_room_wing;
      delete $scope.room_subwings
      $scope.class_room_wing = [];
      if ($scope.selected_branch) {
        $scope.class_room_wing = $scope.class_room_wings.filter($scope.branchIdFilter.bind(null, $scope.selected_branch.id));
      }
      $rootScope.updateChosen();
    }

    $scope.room_wing_select = function(wing_id) {
      $scope.selected_wing = $scope.class_room_wings.filter($scope.idFilter.bind(null, wing_id))[0];
      if ($scope.selected_wing) {
        $scope.room_subwings = $scope.selected_wing.sub_wings;
        $rootScope.updateChosen();
      }
    }

    $scope.room_subwing_select = function(subwing_id) {
      if (subwing_id >= 0) {
        if ($scope.room_subwings.filter($scope.idFilter.bind(null, subwing_id)).length > 0)
          $scope.selected_subwing = $scope.room_subwings.filter($scope.idFilter.bind(null, subwing_id))[0];
        $rootScope.updateChosen();
      }
    }

    $scope.init_class_rooms = function() {

      if (!$scope.class_rooms_new) {
        $scope.count = 1;
        $scope.class_rooms_new = new Array();
        for (var i = 0; i < $scope.count; i++) {
          $scope.class_rooms_new[i] = {

            description: "",
            name: "",
            room_num: "",
            capacity: "",
            room_type_id: "",
            facilities: []
          }
        };
      } else
        $scope.count = $scope.class_rooms_new.length;
    }

    $scope.inc_count_room = function() {
      $scope.class_rooms_new[$scope.count] = {
        description: "",
        name: "",
        room_num: "",
        capacity: "",
        room_type_id: "",
        facilities: []
      }
      $scope.count++;
    }

    $scope.create_room_submit = function() {

      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.add_class_room = {};
      $scope.add_class_room.branch_id = $scope.selected_branch.id;
      $scope.add_class_room.wing_id = $scope.selected_wing.id;
      $scope.add_class_room.sub_wing_id = $scope.selected_subwing.id;
      $scope.add_class_room.rooms = $scope.class_rooms_new;
      ClassRoom.save($scope.subdomain, $scope.domain, $scope.add_class_room)
        .success(function(data) {
          $scope.class_rooms = data.class_rooms;
          $scope.class_room_branches = data.branches;
          $scope.class_room_wings = data.wings;
          $scope.class_room_types = data.room_types;
          $scope.class_room_facilities = data.room_facilities;
          $scope.data = $scope.class_rooms;
          $scope.tableParams2.total($scope.data.length);
          $scope.tableParams2.reload();
          $scope.dismissIt();
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          console.dir(data);
        });
    }
    $scope.edit_room_submit = function() {

      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.add_class_room = {};
      $scope.add_class_room.branch_id = $scope.selected_branch.id;
      $scope.add_class_room.wing_id = $scope.selected_wing.id;
      $scope.add_class_room.sub_wing_id = $scope.selected_subwing.id;
      $scope.add_class_room.rooms = $scope.class_new;
      $scope.add_class_room.rooms.facilities = $scope.edit_faci.facilities;
      ClassRoom.patch($scope.subdomain, $scope.domain, $scope.add_class_room, $scope.add_class_room.rooms.id)
        .success(function(data) {
          $scope.class_rooms = data.class_rooms;
          $scope.class_room_branches = data.branches;
          $scope.class_room_wings = data.wings;
          $scope.class_room_types = data.room_types;
          $scope.class_room_facilities = data.room_facilities;
          $scope.data = $scope.class_rooms;
          $scope.tableParams2.total($scope.data.length);
          $scope.tableParams2.reload();
          $scope.dismissIt();
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          console.dir(data);
        });
    }

    ///////////////////////// Room Type /////////////////////

    $scope.show_Room_types = function() {
      $scope.room_type = {};
      $scope.dialogBoxRoomTypeShow = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/campus/room/room_type/index.html',
        size: 'md',
        backdrop: 'static',
        scope: $scope
      });

    }


    $scope.create_room_type = function() {
      $scope.room_type = {};
      $scope.dialogBoxRoomTypeCreate = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/campus/room/room_type/create.html',
        size: 'md',
        backdrop: 'static',
        scope: $scope
      });

    }

    $scope.create_room_type_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      RoomType.save($scope.subdomain, $scope.domain, $scope.room_type)
        .success(function(data) {
          $scope.class_room_types = data.room_types;
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong. Try again later.");
        }).then(function() {
          $rootScope.updateChosen();
        });
      $scope.dismissRoomTypeCreate();
    }

    $scope.edit_room_type = function(id) {
      $scope.room_type = $scope.class_room_types.filter($scope.idFilter.bind(null, id))[0];
      $scope.dialogBoxRoomTypeEdit = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/campus/room/room_type/edit.html',
        size: 'md',
        backdrop: 'static',
        scope: $scope
      });

    }

    $scope.edit_room_type_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      RoomType.patch($scope.subdomain, $scope.domain, $scope.room_type, $scope.room_type.id)
        .success(function(data) {
          $scope.class_room_types = data.room_types;
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong. Try again later.");
        }).then(function() {
          $rootScope.updateChosen();
        });
      $scope.dismissRoomTypeEdit();
    }


    $scope.dismissRoomTypeCreate = function() {
      $scope.dialogBoxRoomTypeCreate.dismiss('cancel');
    }
    $scope.dismissRoomTypeShow = function() {
      $scope.dialogBoxRoomTypeShow.dismiss('cancel');
    }
    $scope.dismissRoomTypeEdit = function() {
      $scope.dialogBoxRoomTypeEdit.dismiss('cancel');
    }

    ///////////////////////// Room Type End /////////////////////

    ///////////////////////// Facility /////////////////////

    $scope.show_facilities = function() {
      $scope.facility = {};
      $scope.dialogBoxFacilityShow = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/campus/room/facility/index.html',
        size: 'md',
        backdrop: 'static',
        scope: $scope
      });

    }


    $scope.create_facility = function() {
      $scope.facility = {};
      $scope.dialogBoxFacilityCreate = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/campus/room/facility/create.html',
        size: 'md',
        backdrop: 'static',
        scope: $scope
      });

    }

    $scope.create_facility_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      Facility.save($scope.subdomain, $scope.domain, $scope.facility)
        .success(function(data) {
          $scope.class_room_facilities = data.room_facilities;
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong. Try again later.");
        }).then(function() {
          $rootScope.updateChosen();
        });
      $scope.dismissFacilityCreate();
    }

    $scope.edit_facility = function(id) {
      $scope.facility = $scope.class_room_facilities.filter($scope.idFilter.bind(null, id))[0];
      $scope.dialogBoxFacilityEdit = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/campus/room/facility/edit.html',
        size: 'md',
        backdrop: 'static',
        scope: $scope
      });

    }

    $scope.edit_facility_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      Facility.patch($scope.subdomain, $scope.domain, $scope.facility, $scope.facility.id)
        .success(function(data) {
          $scope.class_room_facilities = data.room_facilities;
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong. Try again later.");
        }).then(function() {
          $rootScope.updateChosen();
        });
      $scope.dismissFacilityEdit();
    }

    $scope.dismissFacilityCreate = function() {
      $scope.dialogBoxFacilityCreate.dismiss('cancel');
    }
    $scope.dismissFacilityShow = function() {
      $scope.dialogBoxFacilityShow.dismiss('cancel');
    }
    $scope.dismissFacilityEdit = function() {
      $scope.dialogBoxFacilityEdit.dismiss('cancel');
    }

    ///////////////////////// Room Type End /////////////////////
  }
]);
