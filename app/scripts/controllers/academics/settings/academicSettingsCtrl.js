'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:MailCtrl
 * @description
 * # MailCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')

.controller('AcademicSettingsCtrl', ['$scope', '$filter', '$modal', '$location', '$cookies', 'Global',
  '$resource',
  '$mdDialog',
  'ngTableParams',
  '$rootScope',
  'Transaction',
  'City',
  '$q',
  '$timeout',
  'FileUploader',
  'Session',
  // 'Shift',
  // 'Class',
  // 'Room',
  function($scope, $filter, $modal, $location, $cookies, Global, $resource, $mdDialog, ngTableParams,
    $rootScope, Transaction, City, $q, $timeout, FileUploader, Session) {
    // , Shift, Class, Room) {

    $scope.subdomain = $location.$$host.split('.')[0];
    $scope.domain = Global.rails_domain;
    $scope.permissions = {};
    $scope.page = {
      title: 'Academic Settings',
    };
    ////////////Date/////////

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
    };
    $scope.dp = {};
    $scope.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate', 'yyyy/MM'];
    $scope.format = $scope.formats[1];

    $scope.format_date = function(date, id) {
      var datefilter = $filter('date'),
        formattedDate = datefilter(date, $scope.formats[id]);
      // alert(formattedDate);
      return formattedDate;
    }

    $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.clear = function() {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();


    ///////////////////////////////Session//////////////////////////////
    ////////////////////////////////Start///////////////////////////////
    ////////////////////////////////////////////////////////////////////
    $scope.openStart = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.dp.openedStart = true;
    };

    $scope.openEnd = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.dp.openedEnd = true;
    };

    $scope.show_sessions = function() {
      $rootScope.hideNotify();
      if ($scope.sessions && $scope.sessions.length > 0) {

      } else {
        $scope.load_sessions();
      }
    }
    $scope.load_sessions = function() {


      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      Session.index($scope.subdomain, $scope.domain)
        .success(function(dataRet) {
          $scope.data = dataRet.sessions;
          $scope.sessions = dataRet.sessions;
          for (var i = 0; i < $scope.data.length; i++) {
            if ($scope.data[i].start_date) {
              $scope.data[i].start_date = $scope.format_date(new Date($scope.data[i].start_date), 1);
            }
            if ($scope.data[i].end_date) {
              $scope.data[i].end_date = $scope.format_date(new Date($scope.data[i].end_date), 1);
            }
          }
          $scope.session_branches = dataRet.branches;
          $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
              name: 'asc' // initial sorting
            },
          }, {
            total: $scope.data.length, // length of data
            getData: function($defer, params) {
              // use build-in angular filter
              var filteredData = params.filter() ?
                $filter('filter')($scope.data, params.filter()) :
                $scope.data;
              var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                $scope.data;
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        })
        .error(function(dataRet) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          console.dir(dataRet);
          $scope.errorShow("Something went wrong. Try again later.");
        });
    }
    $scope.parent;

    /////////////////////////// Create ///////////////////////////////

    $scope.create_session = function(size) {
      $scope.session = {};
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/academic_settings/session/dialog/create.html',
        size: size,
        backdrop: 'static',
        windowClass: 'app-modal-window',
        scope: $scope
      });
    }

    $scope.create_session_submit = function() {
      if ($scope.session.selected_branches.length == 0) {
        $scope.errorShow("Select atleast 1 branch.");
        return false;
      }
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.session.branches = [];
      for (var i = 0; i < $scope.session.selected_branches.length; i++) {
        $scope.session.branches[i] = $scope.session.selected_branches[i].id;
      }
      Session.save($scope.subdomain, $scope.domain, $scope.session)
        .success(function(dataRet) {
          $scope.data = dataRet.sessions;
          $scope.sessions = dataRet.sessions;
          for (var i = 0; i < $scope.data.length; i++) {
            if ($scope.data[i].start_date) {
              $scope.data[i].start_date = $scope.format_date(new Date($scope.data[i].start_date), 1);
            }
            if ($scope.data[i].end_date) {
              $scope.data[i].end_date = $scope.format_date(new Date($scope.data[i].end_date), 1);
            }
          }
          $scope.session_branches = dataRet.branches;
          $scope.dismissIt();
          $scope.tableParams.total($scope.data.length);
          $scope.tableParams.reload();
          $scope.session = {};
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $rootScope.showNotify('Session created successfully');
        }).error(function(data) {
          console.dir(data);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong. Try again later.");
        });



    }

    /////////////////////////// Update ///////////////////////////////

    $scope.update_session = function(id, size) {
      $scope.session = jQuery.extend(true, {}, $scope.sessions.filter($scope.idFilter.bind(null, id))[0]);
      $scope.session.selected_branches = new Array();
      var br_ids = $scope.session.branch_ids;
      for (var i = 0; i < br_ids.length; i++) {
        $scope.session.selected_branches.push($scope.session_branches.filter($scope.idFilter.bind(null, br_ids[i].branch_id))[0]);
      }
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/academic_settings/session/dialog/update.html',
        size: size,
        backdrop: 'static',
        windowClass: 'app-modal-window',
        scope: $scope
      });
    }

    $scope.update_session_submit = function() {
      if ($scope.session.selected_branches.length == 0) {
        $scope.errorShow("Select atleast 1 branch.");
        return false;
      }
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.session.branches = [];
      for (var i = 0; i < $scope.session.selected_branches.length; i++) {
        $scope.session.branches[i] = $scope.session.selected_branches[i].id;
      }
      Session.patch($scope.subdomain, $scope.domain, $scope.session, $scope.session.id)
        .success(function(dataRet) {
          $scope.data = dataRet.sessions;
          $scope.sessions = dataRet.sessions;
          for (var i = 0; i < $scope.data.length; i++) {
            if ($scope.data[i].start_date) {
              $scope.data[i].start_date = $scope.format_date(new Date($scope.data[i].start_date), 1);
            }
            if ($scope.data[i].end_date) {
              $scope.data[i].end_date = $scope.format_date(new Date($scope.data[i].end_date), 1);
            }
          }
          $scope.session_branches = dataRet.branches;
          $scope.dismissIt();
          $scope.tableParams.total($scope.data.length);
          $scope.tableParams.reload();
          $scope.session = {};
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $rootScope.showNotify('Session updated successfully');
        }).error(function(data) {
          console.dir(data);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          $scope.errorShow("Something went wrong. Try again later.");
        });

    }

    /////////////////////////// view ///////////////////////////////

    $scope.view_session = function(id, size) {
      $scope.session = jQuery.extend(true, {}, $scope.sessions.filter($scope.idFilter.bind(null, id))[0]);
      $scope.session.selected_branches = new Array();
      var br_ids = $scope.session.branch_ids;
      for (var i = 0; i < br_ids.length; i++) {
        $scope.session.selected_branches.push($scope.session_branches.filter($scope.idFilter.bind(null, br_ids[i].branch_id))[0]);
      }
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/academics/settings/academic_settings/session/dialog/show.html',
        size: size,
        backdrop: 'static',
        windowClass: 'app-modal-window',
        scope: $scope
      });
    }

    //////////////////////// General Functions ////////////////////////////////////


    $scope.dismissIt = function() {
      $scope.dialogBox.dismiss('cancel');
    }

    //////////////////////// Filters ////////////////////////////////////

    $scope.filter = function(element) {
      return element.selected == true;
    }
    $scope.idFilter = function(id, value, index, array) {
      return value.id == id;
    }

  }
]);
