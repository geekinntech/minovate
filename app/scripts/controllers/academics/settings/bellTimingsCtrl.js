'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:MailCtrl
 * @description
 * # MailCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')

.controller('BellTimingsCtrl', ['$scope', '$filter', '$modal', '$location', '$cookies', 'Global',
  '$resource',
  '$mdDialog',
  'ngTableParams',
  '$rootScope',
  'Transaction',
  'City',
  '$q',
  '$timeout',
  'FileUploader',
  'BellTiming',
  function($scope, $filter, $modal, $location, $cookies, Global, $resource, $mdDialog, ngTableParams, $rootScope, Transaction, City, $q, $timeout, FileUploader, BellTiming) {
    $scope.subdomain = $location.$$host.split('.')[0];
    $scope.domain = Global.rails_domain;
    $scope.permissions = {};
    $scope.page = {
      title: 'Bell Timings',
    };

    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.mouse = false;
    $scope.show_spinners = false;

    $scope.options = {
      hstep: [1, 2, 3],
      mstep: [1, 5, 10, 15, 25, 30]
    };

    $scope.ismeridian = true;
    $scope.toggleMode = function() {
      $scope.ismeridian = !$scope.ismeridian;
    };

    $scope.idFilter = function(id, value, index, array) {
      return value.id == id;
    }



    $scope.load_data = function() {


      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      BellTiming.index($scope.subdomain, $scope.domain)
        .success(function(data) {
          $scope.data = data.bells;
          $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
              title: 'asc' // initial sorting
            },
          }, {
            total: $scope.data.length, // length of data
            getData: function($defer, params) {
              // use build-in angular filter
              var filteredData = params.filter() ?
                $filter('filter')($scope.data, params.filter()) :
                $scope.data;
              var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                $scope.data;
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        })
        .error(function(data) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          console.dir(data);
          $scope.errorShow("Something went wrong. Try again later.");
        });
    }
    $scope.load_data();

    $scope.create_bell_timing = function(size) {
      $scope.bell_timing = {
        bell_periods: 1
      }
      $scope.bell_timing.periods = [];
      $scope.bell_timing.periods[0] = {
        code: "",
        start: null,
        end: null,
        length: ""
      }
      $scope.dialogBox = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/bell_timings/dialogs/create.html?bust=' + Math.random().toString(36).slice(2),
        size: size,
        backdrop: 'static',
        windowClass: 'app-modal-window-xl',
        scope: $scope
      });
      $scope.set_timepicker();
    }

    $scope.init_periods = function() {
      delete $scope.bell_timing.periods;
      $scope.bell_timing.periods = [];
      for (var i = 0; i < $scope.bell_timing.bell_periods; i++) {
        $scope.bell_timing.periods[i] = {
          code: "",
          start: null,
          end: null,
          length: ""
        };
      }
      $scope.set_timepicker();
    }

    $scope.set_timepicker = function() {
      setTimeout(function() {
        $('.datetimepicker-bell').datetimepicker({
          pickDate: false
        });
      }, 100);
    }

    $scope.set_value = function(period, id, flag) {
      //$scope.id = id;

      // if (flag == 0)
      //   period.start = $("#" + id + "start").val();
      // else if (flag == 1)
      //   period.end = $("#" + id + "end").val();
      if (period.end && period.start) {
        var start_moment = moment(period.start.toLocaleTimeString(), "hh:mm A");
        var end_moment = moment(period.end.toLocaleTimeString(), "hh:mm A");
        var time = moment(end_moment.subtract(start_moment)); //var time = moment(end_moment.subtract(start_moment).toLocaleTimeString(),"hh:mm A");
        // var time = new TimeSpan(Date.parse(period.end) - Date.parse(period.start)).toString("H:mm");
        period.length = parseInt(time.hours()) * 60 + parseInt(time.minutes());
        if (period.length <= 0 || period.length > 1000)
          period.length = "Invalid Time";
        else {
          var start = period.start;
          var end = period.end;
          for (var i = id + 1; i < $scope.bell_timing.periods.length; i++) {
            //if (!$scope.bell_timing.periods[i].start && !$scope.bell_timing.periods[i].end) {
            $scope.bell_timing.periods[i].length = period.length;
            start = end;
            end = new Date(end.getTime() + (period.length * 60000))

            $scope.bell_timing.periods[i].start = start;
            $scope.bell_timing.periods[i].end = end;
            // }
          }
        }

      }
    }


    $scope.create_bell_timing_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      BellTiming.save($scope.subdomain, $scope.domain, $scope.bell_timing)
        .success(function(data) {
          $scope.data = data.bells;
          $scope.tableParams.total($scope.data.length);
          $scope.tableParams.reload();
          $scope.dismissIt();
          $rootScope.showNotify('Bell Timing successfully created')
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          console.dir(data);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        });
    }

    $scope.view_bell_timing = function(id) {
      $scope.bell_timing = jQuery.extend(true, {}, $scope.data.filter($scope.idFilter.bind(null, id))[0]);
      for (var i = 0; i < $scope.bell_timing.periods.length; i++) {
        $scope.bell_timing.periods[i].start = get_time($scope.bell_timing.periods[i].start);
        $scope.bell_timing.periods[i].end = get_time($scope.bell_timing.periods[i].end);
      }
      $scope.dialogBox = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/bell_timings/dialogs/view.html?bust=' + Math.random().toString(36).slice(2),
        size: 'lg',
        backdrop: 'static',
        windowClass: 'app-modal-window-xl',
        scope: $scope
      });
      $scope.set_timepicker();
    }

    function get_time(timestring) {
      var d = new Date(timestring);
      var hour = d.getHours() == 0 ? 12 : (d.getHours() > 12 ? d.getHours() - 12 : d.getHours());
      var min = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes();
      var ampm = d.getHours() < 12 ? 'AM' : 'PM';
      var time = hour + ':' + min + ' ' + ampm;
      return time;
    }

    $scope.edit_bell_timing = function(id) {
      $scope.bell_timing = jQuery.extend(true, {}, $scope.data.filter($scope.idFilter.bind(null, id))[0]);
      for (var i = 0; i < $scope.bell_timing.periods.length; i++) {
        $scope.bell_timing.periods[i].start = new Date($scope.bell_timing.periods[i].start);
        $scope.bell_timing.periods[i].end = new Date($scope.bell_timing.periods[i].end);
      }
      $scope.dialogBox = $modal.open({
        templateUrl: 'views/tmpl/academics/settings/bell_timings/dialogs/edit.html?bust=' + Math.random().toString(36).slice(2),
        size: 'lg',
        backdrop: 'static',
        windowClass: 'app-modal-window-xl',
        scope: $scope
      });
      $scope.set_timepicker();
    }

    $scope.edit_bell_timing_submit = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      BellTiming.patch($scope.subdomain, $scope.domain, $scope.bell_timing, $scope.bell_timing.id)
        .success(function(data) {
          $scope.data = data.bells;
          $scope.tableParams.total($scope.data.length);
          $scope.tableParams.reload();
          $scope.dismissIt();
          $scope.showAlert('Bell Timing successfully updated','success');
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        }).error(function(data) {
          console.dir(data);
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        });
    }

    $scope.dismissIt = function() {
      $scope.dialogBox.dismiss('cancel');
    }

    $scope.showAlert = function(msg,type) {
      var icon = '';
      if (type === "success")
        icon = 'fa-check';
      else if (type === "warning")
        icon = 'fa-warning';
      $scope.alert = {
        msg: msg,
        type: type,
        timeout: 5000,
        icon: icon,
        closeable: true
      };
      $timeout(function() {
        delete $scope.alert;
      }, $scope.alert.timeout);
    };

    $scope.closeAlert = function(index) {
      delete $scope.alert;//$scope.alerts.splice(index, 1);
    };
  }
]);
