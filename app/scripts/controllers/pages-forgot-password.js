'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:PagesForgotPasswordCtrl
 * @description
 * # PagesForgotPasswordCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')
  .controller('ForgotPasswordCtrl', function($rootScope, $scope, $location, Global, AuthService, $modal, $mdDialog) {


    $scope.subdomain = $location.$$host.split('.')[0];
    $scope.domain = Global.rails_domain;
    $scope.link = $location.$$absUrl.split('#')[0];
    $rootScope.$on('ngDialog.opened', function(e, $dialog) {

      if ($scope.alert) {
        $("#" + $scope.alert.id).children("div.ngdialog-content").addClass("delete");
      }
    });

    $scope.data = {
      link: $scope.link
    };
    $scope.send_recovery_email = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      AuthService.send_recovery_email($scope.subdomain, $scope.domain, $scope.data).success(function(data) {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
        $scope.show_alert("Password Recovery Email has been sent", "Please proceed to your email and follow the link in the email to reset your password");
      }).error(function(data) {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
        $scope.show_alert("Email not found in our database");
      });
    }

    // $scope.show_alert_email_sent = function() {
    //   $('.btn').blur();

    //   var modalInstance = $modal.open({
    //     templateUrl: 'email_sent.html',
    //     controller: 'ModalCtrl',
    //   });

    // }

    $scope.show_alert = function(string, desc) {
      $('.btn').blur();

      $mdDialog.show(
        $mdDialog.alert()
        .parent(angular.element(document.querySelector('#popupContainer')))
        .clickOutsideToClose(true)
        .title(string)
        .content(desc)
        .ariaLabel('Alert')
        .ok('OK')
        //.targetEvent(ev)
      );

    }

    $scope.ok = function() {
      $modalInstance.close($scope.selected.item);
    };

  })

.controller('ModalCtrl', function($scope, $modalInstance, string, flag, $location) {

  $scope.string = string;
  $scope.flag = flag;


  $scope.ok = function() {
    $modalInstance.close();
  };

  $scope.cancel = function() {
    $modalInstance.dismiss('cancel');
  };
})

.controller('ResetPassCtrl', function(
  $scope, $rootScope, $stateParams, $http, $location, $state, $cookies, $modal, $mdDialog, AuthService, Global, $window) {


  $scope.subdomain = $location.$$host.split('.')[0];
  $scope.domain = Global.rails_domain;



  $scope.data = {};
  $scope.reset_password = function() {
    if ((!$scope.user.password) || ($scope.user.password && $scope.user.password.length == 0)) {
      $scope.show_alert("Enter Password");
      return false;
    } else if ((!$scope.user.confirm_password) || ($scope.user.confirm_password && $scope.user.confirm_password.length == 0)) {
      $scope.show_alert("Enter Confirm Password");
      return false;
    } else if ($scope.user.password != $scope.user.confirm_password) {
      $scope.show_alert("Password does'nt match");
      return false;
    } else {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      $scope.user.id = $stateParams.token;
      AuthService.update_password_by_token($scope.subdomain, $scope.domain, $scope.user).success(function(data) {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
        $scope.show_alert("Password has been reset",1);
      }).error(function(data) {
        $.fancybox.hideLoading();
        $.fancybox.helpers.overlay.close();
        $scope.show_alert("Invalid Token");
      });
    }
  }

  $scope.show_alert = function(string,flag) {
    $('.btn').blur();

    var alert = $mdDialog.show(
      $mdDialog.alert()
      .parent(angular.element(document.querySelector('#popupContainer')))
      .clickOutsideToClose(true)
      .title(string)
      .ariaLabel('Alert')
      .ok('OK')
    ).then(function(answer){
      if(flag){
        $location.path('/core/login');
      }
    });

  }

});

//muhammad.haris.4063@gmail.com
