'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:FormsCommonCtrl
 * @description
 * # FormsCommonCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')
  .controller('PermissionsCtrl', ['$scope', '$filter', '$modal', '$location', '$cookies', 'Global',
    '$resource',
    '$mdDialog',
    'Permission',
    'Branch',
    'Role',
    'ngTableParams',
    '$rootScope',

    function($scope, $filter, $modal, $location, $cookies, Global, $resource, $mdDialog, Permission, Branch, Role, ngTableParams, $rootScope) {

      $scope.subdomain = $location.$$host.split('.')[0];
      $scope.domain = Global.rails_domain;
      $scope.permissions = {};
      $scope.permissionToAdd = {};
      $scope.isError = true;
      $scope.isSuccess = true;
      $scope.page = {
        title: 'Permissions',
        //subtitle: ' // list of all the permissions'
      };

      //////////////// Populate Permission Index ///////////////////////////////////////////////
      $scope.populatePermissionTable = function() {

          $.fancybox.showLoading();
          $.fancybox.helpers.overlay.open({
            parent: $('body')
          });
          Permission.index($scope.subdomain, $scope.domain, [])
            .success(function(dataRet) {
              $scope.data = dataRet.permissions;
              $.fancybox.hideLoading();
              $.fancybox.helpers.overlay.close();
              $scope.permission = dataRet.permissions;
              $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 10, // count per page
                sorting: {
                  id: 'asc' // initial sorting
                },
                filter: {
                  // first_name: 'M' // initial filter
                },
              }, {
                total: $scope.data.length, // length of data
                getData: function($defer, params) {
                  // use build-in angular filter
                  var filteredData = params.filter() ?
                    $filter('filter')($scope.data, params.filter()) :
                    $scope.data;
                  var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.data;
                  $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
              });

            })
            .error(function(dataRet) {
              $.fancybox.hideLoading();
              $.fancybox.helpers.overlay.close();
              console.dir(dataRet);
              $scope.errorShow("Something went wrong. Try again later.");

            });

        }
        ///////// get All module values ////////////////////////////////////////
      $scope.getModuleNames = function() {
        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        Permission.get_module_name($scope.subdomain, $scope.domain, [])
          .success(function(createPermissionData) {

            $scope.permissionData = createPermissionData.permission_module_names;
            // console.log($scope.permissionData);
            $scope.allPermissions = [];
            //for all Permissions
            angular.forEach($scope.permissionData, function(permission, permissionIndex) {
              angular.forEach(permission.nested_permission_module, function(subPermission, key) {
                angular.forEach($rootScope.permissionActions, function(action, key) {
                  $scope.allPermissions[$rootScope.permissionNames[permissionIndex].name + "#" + subPermission.name + "#" + action] = false;
                });
              });
            });
            // $scope.printAllPermissions();
            //for check all that permissions correspond to an action
            $scope.groupActionPermissions = [];
            angular.forEach($scope.permissionData, function(permission, permissionIndex) {
              angular.forEach($rootScope.permissionActions, function(action, key) {
                $scope.groupActionPermissions[$rootScope.permissionNames[permissionIndex].name + "#" + action] = false;
              });
            });
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          })
          .error(function(dataRet) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            console.dir(dataRet);
            $scope.errorShow("Something went wrong. Try again later.");

          });
      }

      $scope.getModuleNames();
      $scope.populatePermissionTable();

      ///////// Dismiss Dialog box /////////////////////////////////
      $scope.dismissIt = function() {
        $scope.dialogBox.dismiss('cancel');
      }

      $scope.printAllPermissions = function() {
          console.log($scope.allPermissions);
        }
        /////////////////////add permission//////////////////////////////////////
      $scope.savePermission = function() {

        $scope.permissionToAdd.permission_module_names = $scope.permissionData;
        var temp = {};
        for (var key in $scope.allPermissions) {
          if (key != "clean") {
            var split = key.split('#');
            var groupIndex = $scope.getIndex($rootScope.permissionNames, split[0]);
            $scope.permissionToAdd.permission_module_names[groupIndex].nested_permission_module.filter($scope.nameFilter.bind(null, split[1]))[0][split[2]] = $scope.allPermissions[key];
          }
        };
        // console.log($scope.permissionToAdd);
        Permission.create($scope.subdomain, $scope.domain, $scope.permissionToAdd)
          .success(function(data) {

            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $scope.dismissIt();

            Permission.index($scope.subdomain, $scope.domain, [])
              .success(function(dataRet) {
                $scope.data = dataRet.permissions;
                $scope.tableParams.total($scope.data.length);
                $scope.tableParams.reload();
                $rootScope.showNotify('permission successfully created')
              });

          })
          .error(function(dataRet) {
            console.dir(dataRet);
            $scope.dismissIt();
            $scope.errorShow("Something went wrong. Try again later.");

          });

      }

      ///////////////show create permission popup////////////////////////////////
      $scope.createPopupShow = function(size) {
        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });
        $scope.user = {};
        $scope.dialogBox = $modal.open({
          windowClass: 'app-modal-window',
          templateUrl: 'views/tmpl/settings/Permission/Dialog/create_permission.html',
          size: size,
          windowClass: 'app-modal-window',
          scope: $scope
        });
        $scope.dialogBox.opened.then(function() {

          angular.forEach($scope.permissionData, function(permission, permissionIndex) {
            angular.forEach(permission.nested_permission_module, function(subPermission, key) {
              angular.forEach($rootScope.permissionActions, function(action, key) {
                $scope.allPermissions[$rootScope.permissionNames[permissionIndex].name + "#" + subPermission.name + "#" + action] = false;
              });
            });
          });

          angular.forEach($scope.permissionData, function(permission, permissionIndex) {
            angular.forEach($rootScope.permissionActions, function(action, key) {
              $scope.groupActionPermissions[$rootScope.permissionNames[permissionIndex].name + "#" + action] = false;
            });
          });

          $scope.permissionToAdd.name = "";

          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        });


      }

      ///////////////////////////Filters///////////////////////////////////////////////////
      $scope.idFilter = function(id, value, index, array) {
        return value.id == id;
      }

      $scope.nameFilter = function(name, value, index, array) {
        return value.name == name;
      }

      $scope.getIndex = function(arr, value) {
        for (var i = 0, iLen = arr.length; i < iLen; i++) {

          if (arr[i].name == value) return i;
        }
      }

      ////////////////////////////////////Toggle actions/////////////////////////////////
      $scope.toggleActionForAll = function(permissionName, permissionIndex, action) {

        var select = $scope.groupActionPermissions[permissionName + "#" + action];
        angular.forEach($scope.permissionData[permissionIndex].nested_permission_module, function(subPermission, key) {
          var permissionFullName = permissionName + '#' + subPermission.name + '#' + action;
          $scope.allPermissions[permissionFullName] = select;
        });
        // $scope.printAllPermissions();
      }

      ///////////////////////////Perpare data for update dialog///////////////////////////
      $scope.prepareUpdate = function(id, size, type) {
        $scope.updateId = id;

        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });

        Permission.get($scope.subdomain, $scope.domain, id)
          .success(function(permissionJson) {
            $scope.updatePermissionsData = permissionJson.permission_module_names;
            console.log(permissionJson.permission_module_names);
            $scope.permissionToUpdate = {};
            $scope.permissionToUpdate.permission = {};
            $scope.permissionToUpdate.permission.name = permissionJson.name;
            $scope.permissionToUpdate.permission.id = $scope.updateId;

            $scope.updateAllPermissions = [];
            angular.forEach($scope.updatePermissionsData, function(permission, permissionIndex) {
              angular.forEach(permission.nested_permission_module, function(subPermission, key) {
                angular.forEach($rootScope.permissionActions, function(action, key) {
                  $scope.updateAllPermissions[$rootScope.permissionNames[permissionIndex].name + "#" + subPermission.name + "#" + action] =
                    subPermission[action];
                });
              });
            });


            // for check all that permissions correspond to an action
            $scope.updateGroupActionPermissions = [];

            var permissionFlag = [];
            angular.forEach($scope.updatePermissionsData, function(permission, permissionIndex) {

              angular.forEach($rootScope.permissionActions, function(action, actionKey) {
                $scope.updateGroupActionPermissions[$rootScope.permissionNames[permissionIndex].name + "#" + action] = true;
              });

              angular.forEach(permission.nested_permission_module, function(subPermission, key) {

                angular.forEach($rootScope.permissionActions, function(action, actionKey) {
                  if (!$scope.updateAllPermissions[$rootScope.permissionNames[permissionIndex].name + "#" + subPermission.name + "#" + action]) {
                    $scope.updateGroupActionPermissions[$rootScope.permissionNames[permissionIndex].name + "#" + action] = false;
                  };
                });
              });
            });
            $scope.updatePopupShow(id, size, type);

          })
          .error(function(dataRet) {
            console.dir(dataRet);
            $scope.errorShow("Something went wrong. Try again later.");

          });


      }


      /////////////////////////////////show update modal/////////////////////////////////////////
      $scope.updatePopupShow = function(id, size, type) {

          var templateUrl = 'views/tmpl/settings/Permission/Dialog/view_permission.html';
          if (type === 'update') {
            templateUrl = 'views/tmpl/settings/Permission/Dialog/update_permission.html';
          }
          $scope.user = {};
          $scope.dialogBox = $modal.open({
            windowClass: 'app-modal-window',
            templateUrl: templateUrl,
            size: size,
            windowClass: 'app-modal-window',
            scope: $scope
          });
          $scope.dialogBox.opened.then(function() {

            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          });
        }
        ///////////////////////////////////Toggle actions for update //////////////////////////////////
      $scope.updateToggleActionForAll = function(permissionName, permissionIndex, action) {
          var select = $scope.updateGroupActionPermissions[permissionName + "#" + action];
          angular.forEach($scope.permissionData[permissionIndex].nested_permission_module, function(subPermission, key) {
            var permissionFullName = permissionName + '#' + subPermission.name + '#' + action;
            $scope.updateAllPermissions[permissionFullName] = select;
          });
        }
        /////////////////// Update permissions////////////////////////////////////////////
      $scope.updatePermission = function() {
        $scope.permissionToUpdate.permission_module_names = $scope.updatePermissionsData;

        var temp = {};
        for (var key in $scope.updateAllPermissions) {
          if (key != "clean") {
            var split = key.split('#');
            var groupIndex = $scope.getIndex($rootScope.permissionNames, split[0]);
            console.log(key);
            $scope.permissionToUpdate.permission_module_names[groupIndex].nested_permission_module.filter($scope.nameFilter.bind(null, split[1]))[0][split[2]] = $scope.updateAllPermissions[key];
          }
        };
        // console.log($scope.permissionToUpdate);
        Permission.update($scope.subdomain, $scope.domain, $scope.permissionToUpdate, $scope.updateId)
          .success(function(data) {

            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $scope.dismissIt();
            $rootScope.showNotify('Permissions successfully updated')

          })
          .error(function(dataRet) {
            console.dir(dataRet);
            $scope.dismissIt();
            $scope.errorShow("Something went wrong. Try again later.");

          });

      }



    }
  ]);
