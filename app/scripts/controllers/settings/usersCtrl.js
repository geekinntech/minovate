'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:FormsCommonCtrl
 * @description
 * # FormsCommonCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')

.controller('UsersCtrl', ['$scope', '$filter', '$modal', '$location', '$cookies', 'Global',
  '$resource',
  '$mdDialog',
  'User',
  'Branch',
  'Role',
  'ngTableParams',
  '$q',
  '$rootScope',

  function($scope, $filter, $modal, $location, $cookies, Global, $resource, $mdDialog, User, Branch, Role, ngTableParams, $q, $rootScope) {

    $scope.page = {
      title: 'Users',
      // subtitle: '// list of all the users'
    };
    $scope.subdomain = $location.$$host.split('.')[0];
    $scope.domain = Global.rails_domain;
    $scope.user = {};
    $scope.roles = [];
    $scope.branches = [];

    ////////////////////////// retrieving roles////////////////////////////
    $scope.rolesToShow = function() {
      var def = $q.defer();

      /* http service is based on $q service */
      Role.index($scope.subdomain, $scope.domain)
        .success(function(data) {
          var names = [];

          angular.forEach(data.roles, function(item) {
            names.push({
              'id': item.name,
              'title': item.name
            });

          });

          /* whenever the data is available it resolves the object*/
          def.resolve(names);

        });

      return def;
    };
    /////////////////////populating user table///////////////////
    $scope.populateUserTable = function() {
      $.fancybox.showLoading();
      $.fancybox.helpers.overlay.open({
        parent: $('body')
      });
      User.index($scope.subdomain, $scope.domain, [])
        .success(function(dataRet) {
          $scope.data = dataRet.users;
          $scope.users = dataRet.users;
          $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
              first_name: 'asc' // initial sorting
            },

          }, {
            total: $scope.data.length, // length of data
            getData: function($defer, params) {
              // use build-in angular filter
              var filteredData = params.filter() ?
                $filter('filter')($scope.data, params.filter()) :
                $scope.data;
              var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                $scope.data;
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
        })
        .error(function(dataRet) {
          $.fancybox.hideLoading();
          $.fancybox.helpers.overlay.close();
          console.dir(dataRet);
          $scope.errorShow("Something went wrong. Try again later.");
        });

    }

    $.fancybox.showLoading();
    $.fancybox.helpers.overlay.open({
      parent: $('body')
    });
    ////////////////////retrieving role/////////////////////////////////////
    Role.index($scope.subdomain, $scope.domain)
      .success(function(data) {

        for (var i = 0; i < data.roles.length; i++) {
          $scope.roles.push({
            title: data.roles[i].name,
            id: data.roles[i].id
          });
        }
        Branch.index($scope.subdomain, $scope.domain)
          .success(function(data) {
            $scope.branches = data.branch;
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
          })
          .error(function(data) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            console.dir(data);
            $scope.error();
          });

      })
      .error(function(data) {
        console.dir(data);
        $scope.error();
      });

    $scope.populateUserTable();
    //////////////////////////create  user popup/////////////////////////

    $scope.createPopupShow = function(size) {
      $scope.user = {};
      $scope.isError = false;
      $scope.dialogBox = $modal.open({
        windowClass: 'app-modal-window',
        templateUrl: 'views/tmpl/settings/User/Dialog/create_user.html',
        size: size,
        windowClass: 'app-modal-window',
        scope: $scope
      });
    }

    ///////////////Filter///////////////////////////////////
    $scope.idFilter = function(id, value, index, array) {
      return value.id == id;
    }

    ////////////////////////view popup/////////////////////////
    $scope.viewPopupShow = function(id, size) {
      $scope.user = $scope.users.filter($scope.idFilter.bind(null, id))[0];
      $scope.dialogBox = $modal.open({
        templateUrl: 'views/tmpl/settings/User/Dialog/show_user.html',
        size: size,
        windowClass: 'app-modal-window',
        scope: $scope
      });
    }


    ////////////////////////change password popup////////////////
    $scope.changePassword = function(id, size) {
      $scope.dismissIt();
      $scope.user = $scope.users.filter($scope.idFilter.bind(null, id))[0];
      $scope.dialogBox = $modal.open({
        templateUrl: 'views/tmpl/settings/User/Dialog/change_password.html',
        size: size,
        windowClass: 'app-modal-window',
        scope: $scope
      });
    }

    /////////////////////update popup//////////////////////////////////
    $scope.updatePopupShow = function(id, size) {
        $scope.isError = false;
        $scope.user = $scope.users.filter($scope.idFilter.bind(null, id))[0];
        if ($scope.user.role != null) {
          $scope.user.role_id = $scope.user.role.id;
        }
        $scope.dialogBox = $modal.open({
          templateUrl: 'views/tmpl/settings/User/Dialog/update_user.html',
          size: size,
          windowClass: 'app-modal-window',
          scope: $scope
        });
      }
      /////////////////////update password/////////////////////////////
    $scope.updatePasswordSubmit = function() {

        console.log($scope.user);

        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });

        User.update_password($scope.subdomain, $scope.domain, $scope.user)
          .success(function(data) {


            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();

            if (data.message != null) {
              if (data.message == "Password Updated Successfully...") {
                $rootScope.showNotify('password updated successfully');
                $scope.populateUserTable();
              } else {
                $scope.errorsToShow = data.message;
                $scope.isError = true;
              }

            };

            $scope.user = {};
            $scope.dismissIt();
          })
          .error(function(data) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            if (data.message != null) {
              $scope.errorsToShow = data.message;
              $scope.isError = true;

            } else {
              $scope.dismissIt();
              $scope.errorShow("Something went wrong. Try again later.");
            }


          });
      }
      ///////////////////update submit////////////////////////////
    $scope.updateSubmit = function(role_selected) {


        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });

        var data = {};
        data = $scope.user;
        User.update($scope.subdomain, $scope.domain, $scope.user)
          .success(function(data) {
            console.log(data);

            if (data.errors != null) {
              $.fancybox.hideLoading();
              $.fancybox.helpers.overlay.close();
              $scope.isError = false;
            } else {
              $rootScope.showNotify('user successfully updated');
              User.index($scope.subdomain, $scope.domain, [])
                .success(function(dataRet) {
                  $scope.data = dataRet.users;
                  $scope.users = dataRet.users;
                  $scope.tableParams.total($scope.data.length);
                  $scope.tableParams.reload();
                });
            }
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $scope.user = {};
            $scope.dismissIt();
          })
          .error(function(data) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            if (data.errors && data.errors.length > 0) {
              var errors = "";
              var i;
              for (i = 0; i < data.errors.length - 1; i++) {
                errors = errors.concat(i + 1 + ". ")
                  .concat(data.errors[i])
                  .concat("<br>");
              }
              errors = errors.concat(i + 1 + ". ")
                .concat(data.errors[i]);
              $scope.errorsToShow = errors;
              $scope.isError = true;
            } else {
              $scope.dismissIt();
              $scope.errorShow("Something went wrong. Try again later.");
            }
          });
      }
      ////////////////popup dismiss/////////////////////////
    $scope.dismissIt = function() {
        $scope.dialogBox.dismiss('cancel');
      }
      ////////////////create submit///////////////////////////////
    $scope.createSubmit = function() {

        $.fancybox.showLoading();
        $.fancybox.helpers.overlay.open({
          parent: $('body')
        });

        var data = {};
        data = $scope.user;
        User.create($scope.subdomain, $scope.domain, data)
          .success(function(data) {
            console.log(data);



            if (data.errors != null) {
              $.fancybox.hideLoading();
              $.fancybox.helpers.overlay.close();
              // $scope.errorShow("Something went wrong. Try again later.");
              // angular.forEach data.errors, (errors, field) - >
              //   $scope.userForm[field].$setValidity('server', false)
              // $scope.errors[field] = errors.join(', ')
              $scope.isError = false;
            } else {
              $rootScope.showNotify('user successfully created');


              User.index($scope.subdomain, $scope.domain, [])
                .success(function(dataRet) {
                  $scope.data = dataRet.users;
                  $scope.users = dataRet.users;
                  $scope.tableParams.total($scope.data.length);
                  $scope.tableParams.reload();
                });
            }
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            $scope.user = {};
            $scope.dismissIt();
          })
          .error(function(data) {
            $.fancybox.hideLoading();
            $.fancybox.helpers.overlay.close();
            if (data.errors && data.errors.length > 0) {
              var errors = "";
              var i;
              for (i = 0; i < data.errors.length - 1; i++) {
                errors = errors.concat(i + 1 + ". ")
                  .concat(data.errors[i])
                  .concat("<br>");
              }
              errors = errors.concat(i + 1 + ". ")
                .concat(data.errors[i]);
              $scope.errorsToShow = errors;
              $scope.isError = true;
            } else {
              $scope.dismissIt();
              $scope.errorShow("Something went wrong. Try again later.");
            }
            // angular.forEach data.errors, (errors, field) - >
            //   $scope.userForm[field].$setValidity('server', false)
            // $scope.errors[field] = errors.join(', ')

            // for (var key in data.errors) {
            //   $scope.userForm[key].$setValidity('server', false)
            //     // $scope.errors[key] = data.errors.join('. ')
            // }

          });


      }
      /////////set form//////////////////
    $scope.setForm = function(form) {
        $scope.userForm = form;
      }
      /////////show Error Dialog///////////////////////
    $scope.errorShow = function(data) {
      $('.btn')
        .blur();
      if (!$scope.error_dailog) {
        $scope.error_dailog = $mdDialog.show(
          $mdDialog.alert()
          .parent(angular.element(document.querySelector('body')))
          .clickOutsideToClose(true)
          .title('Error')
          .content(data)
          .ok('OK')
        );
      }
    }
  }
]);
