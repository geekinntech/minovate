'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:MailCtrl
 * @description
 * # MailCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')

.controller('LogsCtrl', function($scope, $location, Global, LogService, ngTableParams, $filter, $mdDialog) {
  $scope.page = {
    title: 'Logs',
    //subtitle: 'list of all logs'
  };


  $scope.subdomain = $location.$$host.split('.')[0];
  $scope.domain = Global.rails_domain;




  $scope.get_logs = function() {
    $.fancybox.showLoading();
    $.fancybox.helpers.overlay.open({
      parent: $('body')
    });
    $scope.logs = [];
    LogService.search_logs($scope.subdomain, $scope.domain, $scope.start_date.toDateString(), $scope.end_date.toDateString()).success(function(data) {
      $scope.logs = data.logs;


      $.fancybox.hideLoading();
      $.fancybox.helpers.overlay.close();
    }).error(function(data) {
      $scope.show_alert("No logs found");
      $.fancybox.hideLoading();
      $.fancybox.helpers.overlay.close();
    }).then(function() {
      $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
          name: 'asc' // initial sorting
        }
      }, {
        total: $scope.logs.length, // length of data
        getData: function($defer, params) {
          // use build-in angular filter
          var orderedData = params.sorting() ?
            $filter('orderBy')($scope.logs, params.orderBy()) :
            $scope.logs;

          $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
      });
    });
  }


  $scope.today = function() {
    $scope.dt = new Date();
  };



  $scope.clear = function() {
    $scope.dt = null;
  };

  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
  };

  $scope.toggleMin = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();

  $scope.open1 = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened1 = true;
  };

  $scope.open2 = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened2 = true;
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    'class': 'datepicker'
  };

  $scope.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[1];


  $scope.show_alert = function(string) {
    $('.btn').blur();

    $mdDialog.show(
      $mdDialog.alert()
      .parent(angular.element(document.querySelector('#popupContainer')))
      .clickOutsideToClose(true)
      .title(string)
      .ariaLabel('Alert')
      .ok('OK')
    );

  }

});
